#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Rename this file to environment.py and specify appropriate config values

import os

os.environ['ALLOWED_HOSTS'] = 'localhost;127.0.0.1;[::1];kaskosago.boga.myjino.ru'
os.environ['DJANGO_DEBUG'] = 'TRUE'
os.environ['SECRET_KEY'] = 'azcwk#l*f8yw^+w^0vp-wn$%*893cvr)lbox%pw9)s!i=!icm#'
os.environ['STATIC_ROOT'] = 'static'
os.environ['DB_HOST'] = 'db'
os.environ['DB_NAME'] = 'docker'
os.environ['DB_USER'] = 'docker'
os.environ['DB_PASS'] = 'docker'
os.environ['DOMAIN'] = 'kaskosago.boga.myjino.ru'
os.environ['DEFAULT_FROM_EMAIL'] = 'ivan.shnurchenko+insurance_register@gmail.com'
os.environ['STATISTICS_MAILS'] = '''
    'Ivan Shnurchenko <croaked@yandex.ru>';
    'Ivan Shnurchenko <ivan.shnurchenko+notify@gmail.com>';
'''
os.environ['STATISTICS_SENDER_MAIL'] = 'Робот статистики <robot@boga.myjino.ru>'

os.environ['SHOW_DEBUG_TOOLBAR'] = 'FALSE'

os.environ['EMAIL_BACKEND'] = 'console'

# File has to be specified for this backend
# os.environ['EMAIL_BACKEND'] = 'filebased'
# os.environ['EMAIL_FILE_PATH'] = ''

# Real-world email backend
# os.environ['EMAIL_BACKEND'] = 'smtp'
# os.environ['EMAIL_HOST'] = 'smtp.jino.ru'
# os.environ['EMAIL_HOST_USER'] = 'robot@boga.myjino.ru'
# os.environ['EMAIL_HOST_PASSWORD'] = 'ndk9Cj4j&#b8s/M'
# os.environ['EMAIL_PORT'] = 465
# os.environ['EMAIL_USE_TLS'] = True
