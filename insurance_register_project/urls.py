#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.contrib import admin, auth
from django.conf import settings
from django.urls import reverse

handler403 = 'insurance_register_app.views.handler403_view'
handler404 = 'insurance_register_app.views.handler404_view'
handler500 = 'insurance_register_app.views.handler500_view'

urlpatterns = [
    # url(r'^accounts/login/$', auth.login,
    #     {'template_name': 'admin/login.html'}),
    # url(r'^accounts/logout/$', auth.logout, {'next_page': reverse('index')}),
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^admin/', admin.site.urls),
]

if settings.DEBUG:
    from django.conf.urls.static import static
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

    import debug_toolbar
    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]

urlpatterns += [
    url(r'^', include('insurance_register_app.urls'))
]
