#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


env_file = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                    'environment.py')
if os.path.isfile(env_file):
    from . environment import *

SECRET_KEY = os.getenv('SECRET_KEY')
DEBUG = 'TRUE' == (os.getenv('DJANGO_DEBUG', 'false')).upper()
#DEBUG = True

allowed_hosts_str = os.getenv('ALLOWED_HOSTS', '')
ALLOWED_HOSTS = allowed_hosts_str.split(';')

DOMAIN = os.getenv('DOMAIN', 'localhost')

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'debug_toolbar',
    'insurance_register_app',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'insurance_register_app.middleware.RequestMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'debug_toolbar.middleware.DebugToolbarMiddleware',
]

INTERNAL_IPS = [
    '127.0.0.1',
    '192.168.1.2',
    '192.168.1.3',
]

ROOT_URLCONF = 'insurance_register_project.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'insurance_register_app.context_processors.general_menu',
            ],
        },
    },
]

WSGI_APPLICATION = 'insurance_register_project.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'HOST': os.getenv('DB_HOST'),
        'PORT': os.getenv('DB_PORT') or "8082",
        'NAME': os.getenv('DB_NAME'),
        'USER': os.getenv('DB_USER'),
        'PASSWORD': os.getenv('DB_PASS'),
        'OPTIONS': {
            # 'charset': 'utf8mb4',
            'init_command': "SET sql_mode='STRICT_TRANS_TABLES'; SET default_storage_engine=INNODB; SET NAMES utf8mb4; ",
        },
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'ru'
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

TIME_ZONE = 'Europe/Moscow'
USE_I18N = True
USE_L10N = True
USE_TZ = True


STATIC_URL = '/static/'
STATIC_ROOT = os.getenv('STATIC_ROOT')
MEDIA_URL = '/upload/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

APPEND_SLASH = True
LOGOUT_REDIRECT_URL = '/'

AUTH_USER_MODEL = 'insurance_register_app.User'

DEFAULT_FROM_EMAIL = os.getenv('DEFAULT_FROM_EMAIL')
if not DEFAULT_FROM_EMAIL:
    raise Exception('DEFAULT_FROM_EMAIL is missed')

statistics_mails_str = os.getenv('STATISTICS_MAILS', '')
STATISTICS_MAILS = [i for i in [c.strip() for c in statistics_mails_str.split(';')] if len(i) > 0]
if not STATISTICS_MAILS:
    raise Exception('STATISTICS_MAILS is missed')

STATISTICS_SENDER_MAIL = os.getenv('STATISTICS_SENDER_MAIL', 'robot@' + DOMAIN)

# Email backend and its config
EMAIL_BACKEND_SMTP = 'smtp'
EMAIL_BACKEND_FILEBASED = 'filebased'
EMAIL_BACKEND_CONSOLE = 'console'

EMAIL_BACKEND = os.getenv('EMAIL_BACKEND', EMAIL_BACKEND_SMTP)

if EMAIL_BACKEND_FILEBASED == EMAIL_BACKEND:
    EMAIL_FILE_PATH = os.getenv('EMAIL_FILE_PATH')
    if EMAIL_FILE_PATH is None:
        raise Exception('File path for file email backend is not specified')

if EMAIL_BACKEND_SMTP == EMAIL_BACKEND:
    EMAIL_HOST = os.getenv('EMAIL_HOST')
    EMAIL_HOST_USER = os.getenv('EMAIL_HOST_USER')
    EMAIL_HOST_PASSWORD = os.getenv('EMAIL_HOST_PASSWORD')
    EMAIL_PORT = os.getenv('EMAIL_PORT')
    EMAIL_USE_TLS = 'TRUE' == os.getenv('EMAIL_USE_TLS', 'True').upper()

    if not EMAIL_HOST or \
       not EMAIL_HOST_USER or \
       not EMAIL_HOST_PASSWORD or \
       not EMAIL_PORT:
        raise Exception('SMPT credentials for smpt email backend is not specified')

EMAIL_BACKEND = 'django.core.mail.backends.' + EMAIL_BACKEND + '.EmailBackend'


def show_toolbar(request):
    return 'TRUE' == os.getenv('SHOW_DEBUG_TOOLBAR', False).upper()

DEBUG_TOOLBAR_CONFIG = {
    'SHOW_TOOLBAR_CALLBACK': 'insurance_register_project.settings.show_toolbar',
}
