#!/usr/bin/env bash
export ALLOWED_HOSTS='localhost;127.0.0.1;[::1]'
export DJANGO_DEBUG='true'
export SECRET_KEY='you cant stop me'
export STATIC_ROOT="static"
export SHOW_DEBUG_TOOLBAR="False"
export DEFAULT_FROM_EMAIL="username@domain"
export STATISTICS_MAILS="
    'Foo Bar <foobar@domain>';
    'Bar Foo <barfoo@domain>';
"
export STATISTICS_SENDER_MAIL="Stats robot <robot@domain>"

# Development backend
export EMAIL_BACKEND='console'

# File has to be specified for this backend
#export EMAIL_BACKEND='filebased'
#export EMAIL_FILE_PATH=''

# Real-world email backend
#export EMAIL_BACKEND='smtp'
#export EMAIL_HOST='smtp.jino.ru'
#export EMAIL_HOST_USER='robot@boga.myjino.ru'
#export EMAIL_HOST_PASSWORD='ndk9Cj4j&#b8s/M'
#export EMAIL_PORT="465"
#export EMAIL_USE_TLS="True"

docker-compose config
docker-compose up --build -d # > /dev/null 2>&1
