# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-11 10:35


import django.contrib.auth.models
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('auth', '0008_alter_user_username_max_length'),
    ]

    operations = [
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('password', models.CharField(max_length=128, verbose_name='password')),
                ('last_login', models.DateTimeField(blank=True, null=True, verbose_name='last login')),
                ('username', models.CharField(default='', max_length=200)),
                ('first_name', models.CharField(max_length=50)),
                ('last_name', models.CharField(max_length=250)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('is_active', models.BooleanField()),
                ('is_staff', models.IntegerField(default=False)),
                ('is_superuser', models.IntegerField(default=False)),
                ('groups', models.ManyToManyField(blank=True, help_text='The groups this user belongs to. A user will get all permissions granted to each of their groups.', related_name='user_set', related_query_name='user', to='auth.Group', verbose_name='groups')),
                ('user_permissions', models.ManyToManyField(blank=True, help_text='Specific permissions for this user.', related_name='user_set', related_query_name='user', to='auth.Permission', verbose_name='user permissions')),
            ],
            options={
                'verbose_name': 'User',
                'verbose_name_plural': 'Users',
            },
            managers=[
                ('objects', django.contrib.auth.models.UserManager()),
            ],
        ),
        migrations.CreateModel(
            name='Policy',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('edited_at', models.DateTimeField(auto_now=True)),
                ('number', models.CharField(max_length=300)),
                ('date_issued', models.DateTimeField(blank=True, null=True)),
                ('comment', models.TextField(blank=True, max_length=21500, null=True)),
            ],
            options={
                'verbose_name': '\u041f\u043e\u043b\u0438\u0441',
                'verbose_name_plural': '\u041f\u043e\u043b\u0438\u0441\u044b',
                'permissions': [('view_statistics', 'View statistics'), ('issue_policy', 'Issue policy'), ('bulk_add_policy', 'Bulk add policy')],
            },
        ),
    ]
