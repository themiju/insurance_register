# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-13 16:36


from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('insurance_register_app', '0004_auto_20161213_1919'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='date_joined',
            field=models.DateTimeField(auto_now_add=True, default=django.utils.timezone.now, verbose_name='Date of join'),
            preserve_default=False,
        ),
    ]
