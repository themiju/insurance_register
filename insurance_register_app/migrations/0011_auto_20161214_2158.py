# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2016-12-14 18:58


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('insurance_register_app', '0010_auto_20161214_2150'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='policy',
            options={'permissions': [('view_statistics', 'View statistics'), ('issue_policy', 'Issue policy'), ('bulk_add_policy', 'Bulk add policy')], 'verbose_name': 'policy', 'verbose_name_plural': 'policies'},
        ),
        migrations.AlterField(
            model_name='policy',
            name='branch',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='insurance_register_app.Branch', verbose_name='branch'),
        ),
        migrations.AlterField(
            model_name='policy',
            name='comment',
            field=models.TextField(blank=True, max_length=21500, null=True, verbose_name='comment'),
        ),
        migrations.AlterField(
            model_name='policy',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='created at'),
        ),
        migrations.AlterField(
            model_name='policy',
            name='date_issued',
            field=models.DateTimeField(blank=True, null=True, verbose_name='date issued'),
        ),
        migrations.AlterField(
            model_name='policy',
            name='edited_at',
            field=models.DateTimeField(auto_now=True, verbose_name='edited at'),
        ),
        migrations.AlterField(
            model_name='policy',
            name='number',
            field=models.CharField(max_length=250, unique=True, verbose_name='number'),
        ),
    ]
