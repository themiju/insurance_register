# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-06-20 16:40


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insurance_register_app', '0118_remove_auditlog_edited_by'),
    ]

    operations = [
        migrations.AddField(
            model_name='auditlog',
            name='edited_by',
            field=models.PositiveIntegerField(default=0, verbose_name='Editor'),
            preserve_default=False,
        ),
    ]
