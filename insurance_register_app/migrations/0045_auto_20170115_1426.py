# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-01-15 11:26


from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('insurance_register_app', '0044_policy_type'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='email',
        ),
        migrations.AlterField(
            model_name='user',
            name='username',
            field=models.CharField(default='', max_length=200, unique=True, verbose_name='User name'),
        ),
    ]
