# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-04-17 16:17


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('insurance_register_app', '0090_auto_20170417_1835'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='branch',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='insurance_register_app.Branch', verbose_name='Branch'),
            preserve_default=False,
        ),
    ]
