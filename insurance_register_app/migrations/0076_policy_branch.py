# -*- coding: utf-8 -*-
# Generated by Django 1.10.4 on 2017-02-19 09:47


from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('insurance_register_app', '0075_utilizationact_parent_act'),
    ]

    operations = [
        migrations.AddField(
            model_name='policy',
            name='branch',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='insurance_register_app.Branch', verbose_name='Branch'),
            preserve_default=False,
        ),
    ]
