#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import url, include
from django.contrib.auth.decorators import permission_required, login_required

from .views import \
    import_policies_act_confirm_delivery, \
    import_policies_act_in_branch, import_policies_act_delete, \
    import_policies_print_dispatch_act, import_policies_act_view, policies_import, \
    PoliciesListView, ClientListView, client_edit, main_page, \
    policy_issue, PaperEdit, report, invoices_report, InsuredEdit, \
    api_recipients_list, api_policies_list, api_policy_get, \
    utilization_act_view, UtilizationActsListView, utilization_act_add, \
    utilization_act_del, api_utilization_acts_list, api_utilization_act_edit, \
    ExpensesListView, ExpenseCreate, ExpenseDelete, ExpenseUpdate, stats_view, \
    PolicyDelete, policy_corrupted, policy_cancel, docs, VehicleBrandListView, \
    VehicleBrandCreate, VehicleBrandDelete, VehicleBrandUpdate, \
    import_policies_print_accept_act, client_add, utilization_act_print, \
    api_policy_edit, ImportantPoliciesListView, \
    api_unseen_important_policies_list, ClientDelete, \
    audit_logs, InsuredAuditDetailView

paper_urls = None
vehicle_brands_urls = [
    url(r'^dobavit/?',
        permission_required('insurance_register_app.add_vehiclebrand')(
            VehicleBrandCreate.as_view()), name='add'),
    url(r'^(?P<pk>[0-9]+)/udalit/?',
        permission_required('insurance_register_app.delete_vehiclebrand')(
            VehicleBrandDelete.as_view()), name='delete'),
    url(r'^(?P<pk>[0-9]+)/?',
        permission_required('insurance_register_app.change_vehiclebrand')(
            VehicleBrandUpdate.as_view()), name='edit'),
    url(r'^',
        login_required(VehicleBrandListView.as_view()), name='list'),
]
audit_logs_urls = [
    url(r'^(?P<audited_type>[0-9]+)/(?P<audited_id>[0-9]+)/?', InsuredAuditDetailView.as_view(), name='details'),
    url(r'^', audit_logs, name='list'),
]
policies_import_urls = [
    url(r'^akt/', include([url(r'^(?P<act_id>[0-9]+)/udalit$',
                               import_policies_act_delete, name='delete'),
                           url(r'^(?P<act_id>[0-9]+)/podtverdit$',
                               import_policies_act_confirm_delivery,
                               name='approve'),
                           url(r'^(?P<act_id>[0-9]+)/v_filiale$',
                               import_policies_act_in_branch,
                               name='in_branch'),
                           url(r'^(?P<act_id>[0-9]+)/pechat_otpravki$',
                               import_policies_print_dispatch_act, name='dispatch_print'),
                           url(r'^(?P<act_id>[0-9]+)/pechat_priema$',
                               import_policies_print_accept_act, name='accept_print'),
                           url(r'^(?P<act_id>[0-9]+)$',
                               import_policies_act_view, name='view'), ],
                          namespace='act')),
    url(r'^', policies_import, name='index'), ]

utilization_acts = [
    url(r'^dobavit$', utilization_act_add, name='add'),
    url(r'^(?P<act_id>[0-9]+)/udalit/?', utilization_act_del, name='delete'),
    url(r'^(?P<act_id>[0-9]+)/pechat/?', utilization_act_print, name='print'),
    url(r'^(?P<act_id>[0-9]+)/?', utilization_act_view, name='view'),
    url(r'^', UtilizationActsListView.as_view(), name='list'),
]

policies_urls = [
    url(r'^(?P<pk>[0-9]+)/udalit/?', PolicyDelete.as_view(), name='delete'),
    url(r'^(?P<policy_id>[0-9]+)/isporchen/?', policy_corrupted,
        name='corrupt'),
    url(r'^(?P<policy_id>[0-9]+)/rastorjen/?', policy_cancel,
        name='cancel'),
    url(r'^(?P<policy_id>[0-9]+)/?', policy_issue, name='edit'),
    url(r'^(?P<insured_type>\w+)/(?P<insured_id>[0-9]+)/(?P<policy_type>\w+)(?:/prolongs/(?P<prolong_id>\w+))?/?',
        policy_issue, name='add'),
    url(r'^oshibki', ImportantPoliciesListView.as_view(), name='important-list'),
    url(r'^', PoliciesListView.as_view(), name='list'),
]

urlpatterns = [
    url(r'^import_polisov/', include(policies_import_urls, namespace='policies_import')),
    url(r'^policies/', include(policies_urls, namespace='policies')),

    url(r'^rashody/', include([
        url(r'^add/?', ExpenseCreate.as_view(), name='add'),
        url(r'^(?P<pk>[0-9]+)/udalit/?', ExpenseDelete.as_view(), name='delete'),
        url(r'^(?P<pk>[0-9]+)/?', ExpenseUpdate.as_view(), name='edit'),
        url(r'^', ExpensesListView.as_view(), name='list'),
    ], namespace='expenses')),

    url(r'^akty_spisania/', include(utilization_acts, namespace='utilization_act')),

    url(r'^clienty/', include([
        url(r'^$', ClientListView.as_view(), name='list'),
        url(r'^(?P<client_id>[0-9]+)/', include([
            url(r'^$', client_edit, name='edit'),
            url(r'^udalit$', ClientDelete.as_view(), name='delete'),

            url(r'^dokumenty/', include([
                url(r'^(?P<paper_id>[0-9]+)/delete',
                    PaperEdit.as_view(), {'delete': True}, name='delete'),
                url(r'^(?:(?P<paper_id>[0-9]+))?', PaperEdit.as_view(), name='edit'),
            ], namespace='papers')),

            url(r'^insured/', include([
                url(r'^(?P<insured_type>\w+)/(?:(?P<insured_id>[0-9]+))/delete',
                    InsuredEdit.as_view(), {'delete': True}, name='delete'),
                url(r'^(?P<insured_type>\w+)/(?:(?P<insured_id>[0-9]+))?',
                    InsuredEdit.as_view(), name='edit'),
            ], namespace='insured')),
        ])),
        url(r'^dobavit/(?P<client_type>\w)$', client_add, name='add'),
    ], namespace='clients')),

    url(r'^api/', include([
        url(r'^policies$', api_policies_list, name='api-policies-list'),
        url(r'^policies/unseen-important$', api_unseen_important_policies_list, name='api-unseen-important-policies-list'),
        url(r'^policies/(?P<policy_id>[0-9]+)/$', api_policy_get, name='api-policy-get'),
        url(r'^policies/(?P<policy_id>[0-9]+)/edit$', api_policy_edit, name='api-policy-edit'),
        url(r'^recipients$', api_recipients_list, name='api-recipients-list'),
        url(r'^utilization-acts$', api_utilization_acts_list, name='api-utilization-acts-list'),
        url(r'^utilization-act/edit$', api_utilization_act_edit,
            name='api-utilization-act-edit'),
    ])),

    url(r'^brendy/', include(vehicle_brands_urls, namespace='vehicle_brands')),

    url(r'^report', report, name='report'),
    url(r'^scheta_report', invoices_report, name='invoices_report'),
    url(r'^documentacia$', docs, name='docs'),
    url(r'^statistika$', stats_view, name='view_statistics'),
    url(r'^jurnal/', include(audit_logs_urls, namespace='logs')),
    url(r'^$', main_page, name='index'),
]
