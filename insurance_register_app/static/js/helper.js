/**
 * Created by miju on 04.01.17.
 */

var Helper = {
    'addParamsToUrl': function (url, params) {
        var firstPosition = url.indexOf('?');
        var cur_params = {};
        // Retrieve current params to hash-map
        if (firstPosition > 0) {
            var hashes = decodeURIComponent(url.substr(firstPosition+1)).split('&');
            hashes.forEach(function (hash) {
                hash = hash.split('=');
                cur_params[hash[0]] = hash[1];
            });
        }
        // Renew current params with new values
        for (var field in params) {
            if (params.hasOwnProperty(field)) {
                cur_params[field] = params[field];
            }
        }
        // Create new params string chunks for url
        hashes = [];
        for (var cur_field in cur_params) {
            if (cur_params.hasOwnProperty(cur_field)) {
                hashes.push(cur_field + '=' + cur_params[cur_field]);
            }
        }

        // If there is not any params in url
        if (firstPosition < 0) firstPosition = url.length;

        url = url.substr(0, firstPosition) + '?' + hashes.join('&');

        return url;
    }
};
