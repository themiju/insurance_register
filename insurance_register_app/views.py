#!/usr/bin/env python
# -*- coding: utf-8 -*-
import json
from collections import OrderedDict
from datetime import datetime
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.utils import translation
from django.conf import settings

from django.contrib.auth.decorators import login_required, permission_required
from django.core.exceptions import ObjectDoesNotExist, NON_FIELD_ERRORS
from django.db.models import Q, Count, Sum, Case, CharField, Value, When
from django.db.models.functions import Concat
from django.http import HttpResponseForbidden
from django.http import HttpResponseRedirect
from django.http import JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse_lazy
from django.utils.decorators import method_decorator
from django.utils.translation import ugettext_lazy as _
from django.views import View
from django.views.decorators.http import require_POST
from django.views.generic import ListView, DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView

from .forms import PoliciesImportForm, PolicyForm, PaperForm, \
    PoliciesReportForm, InvoicesReportForm, InsuredForm, ExpenseEditForm, \
    ExpensesListForm, StatisticsListForm, PolicyCorruptedForm, PolicyCancelForm, \
    VehicleBrandEditForm, ClientOrganizationForm, ClientPersonForm, \
    ClientListForm

from .models import Insured, Expense, VehicleBrand, \
    Policy, Branch, InsuranceCompany, PoliciesImportAct, \
    User, Client, Paper, InsuredVehicle, UtilizationAct, Statistics, \
    get_object_by_class_and_pk, PROLONGED_TYPES, PROLONGED_TYPE_N, PROLONGED_TYPE_P, \
    AuditLog, AuditedObjectMixin, Invoice


@login_required
def main_page(request):
    ctx = {}
    user = request.user

    return redirect(reverse_lazy(user.start_url))


@method_decorator(login_required, name='dispatch')
class PoliciesListView(ListView):
    model = Policy
    paginate_by = 20
    context_object_name = 'policies'
    template_name = 'policies/list.html'

    def get_queryset(self):
        policies = Policy.objects
        branch = self.request.user.filtering_branch
        if branch:
            policies = policies.filter(branch=branch)

        return policies.all()

        # def policies_list(self, request):
        #     ctx = {}
        #     user = request.user
        #
        #     policies = Policy.objects
        #     if user.is_operator:
        #         acts = PoliciesImportAct.objects.filter(branch=user.branch)
        #         policies = policies.filter(act__in=acts)
        #     policies = policies.all()
        #
        #     ctx['policies'] = policies
        #
        #     return render(request, , ctx)


@method_decorator(login_required, name='dispatch')
class ImportantPoliciesListView(ListView):
    model = Policy
    paginate_by = 20
    context_object_name = 'policies'
    template_name = 'policies/list_important.html'

    def get_queryset(self):
        policies = Policy.objects.filter(important=True)
        branch = self.request.user.filtering_branch
        if branch:
            policies = policies.filter(branch=branch)

        self.request.user.importants_last_reading_time = datetime.now()
        self.request.user.save()

        return policies.all()


@login_required
def policy_corrupted(request, policy_id):
    ctx = {}
    user = request.user
    policy = get_object_or_404(Policy, pk=policy_id)
    # if not request.user.check_user_access(policy.branch):
    #     return HttpResponseForbidden()

    form = PolicyCorruptedForm()

    if policy.STATUS_CORRUPTED == policy.status:
        message = _('Policy "%s" is corrupted') % policy
    elif policy.STATUS_CORRUPTED_CANDIDAT == policy.status:
        message = _('Confirm that policy "%s" is corrupted') % policy
    else:
        message = _('Declare policy "%s" as corrupted') % policy

    if request.method == 'POST':
        form = PolicyCorruptedForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['agree']:
                if policy.STATUS_CORRUPTED_CANDIDAT == policy.status:
                    policy.status = policy.STATUS_CORRUPTED
                else:
                    policy.status = policy.STATUS_CORRUPTED_CANDIDAT
            else:
                if policy.insured:
                    policy.status = policy.STATUS_ISSUED
                else:
                    policy.status = policy.STATUS_NEW

            policy.save()
            return redirect('policies:edit', policy_id=policy.id)

    ctx['form'] = form
    ctx['policy'] = policy
    ctx['message'] = message

    return render(request, 'policies/trash.html', ctx)

@login_required
def policy_cancel(request, policy_id):
    ctx = {}
    user = request.user
    policy = get_object_or_404(Policy, pk=policy_id)
    # if not request.user.check_user_access(policy.branch):
    #     return HttpResponseForbidden()

    form = PolicyCancelForm()

    if policy.STATUS_CANCELED == policy.status:
        message = _('Policy "%s" is canceled') % policy
    else:
        message = _('Declare policy "%s" as canceled') % policy

    if request.method == 'POST':
        form = PolicyCancelForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['agree']:
                policy.status = policy.STATUS_CANCELED
            else:
                policy.status = policy.STATUS_ISSUED

            policy.save()
            return redirect('policies:edit', policy_id=policy.id)

    ctx['form'] = form
    ctx['policy'] = policy
    ctx['message'] = message

    return render(request, 'policies/cancel.html', ctx)


@require_POST
@login_required
def utilization_act_add(request):
    res = {}
    act = UtilizationAct()
    act.save()
    res['status'] = 'ok'

    return JsonResponse(res)


@require_POST
@login_required
def utilization_act_del(request, act_id):
    res = {}
    try:
        act = UtilizationAct.objects.get(pk=act_id)
        act.delete()
        res['status'] = 'ok'
    except ObjectDoesNotExist:
        res['status'] = 'error'
        res['message'] = _('Act not found: ') + act_id

    return JsonResponse(res)


@login_required
def utilization_act_view(request, act_id):
    ctx = {}
    act = get_object_or_404(UtilizationAct, pk=act_id)  # type: UtilizationAct
    ctx['act'] = act

    return render(request, 'utilization_act/view.html', ctx)


@login_required()
def utilization_act_print(request, act_id):
    act = get_object_or_404(UtilizationAct, pk=act_id)

    policies = sorted(act.policies_all, key=lambda policy: policy.full_number)

    return render(request, 'utilization_act/print.html', {
        'act': act,
        'policies': policies,
    })


@require_POST
@login_required
def api_utilization_act_edit(request):
    res = {}
    post_data = json.loads(request.body)
    act_pk = None
    act = None
    message = _('Successfully removed from any acts')
    if ('act_id' in post_data) and post_data['act_id']:
        act_pk = post_data['act_id']
        act = get_object_or_404(UtilizationAct,
                                pk=act_pk)  # type: UtilizationAct
        message = _("Successfully attached to act #%d") % act.pk

    if 'policies' in post_data:
        for policy_id in list(post_data['policies'].values()):
            policy = Policy.objects.get(pk=policy_id)
            policy.utilization_act = act
            if act is None:
                if policy.insured:
                    policy.status = policy.STATUS_ISSUED
                else:
                    policy.status = policy.STATUS_NEW
            policy.save()

    if 'acts' in post_data:
        for child_act_id in list(post_data['acts'].values()):
            if child_act_id != act_pk:
                child_act = UtilizationAct.objects.get(
                    pk=child_act_id)  # type: UtilizationAct
                child_act.parent_act = act
                child_act.save()

    res['status'] = 'ok'
    res['message'] = message

    return JsonResponse(res)


@method_decorator(login_required, name='dispatch')
class UtilizationActsListView(ListView):
    model = UtilizationAct
    paginate_by = 20
    ordering = '-edited_at'
    context_object_name = 'acts'
    template_name = 'utilization_act/list.html'


@method_decorator(login_required, name='dispatch')
class PolicyDelete(DeleteView):
    model = Policy
    template_name = 'policies/delete.html'

    def delete(self, request, *args, **kwargs):
        policy = self.get_object()  # type: Policy
        # if not request.user.check_user_access(policy.branch):
        #     return HttpResponseForbidden()
        url = reverse_lazy('clients:edit', kwargs={
            'client_id': policy.insured.client.pk,
        })
        if not policy.is_readonly:
            policy.delete_method()

        return HttpResponseRedirect(url)


@login_required
def policy_issue(request, insured_type=None, insured_id=None, policy_type=None,
                 policy_id=None, prolong_id=None):
    ctx = {}

    insured = None
    if policy_id:
        policy = get_object_or_404(Policy, pk=policy_id)  # type: Policy
        # if not request.user.check_user_access(policy.branch):
        #     return HttpResponseForbidden()

        if hasattr(policy, 'insured') and policy.insured:
            insured = policy.insured
        if request.GET.get('insured_id'):
            insured = get_object_or_404(InsuredVehicle,
                                        pk=request.GET.get('insured_id'))
            policy.insured = insured

        insured_type = policy.insured.__class__.__name__
        insured_id = policy.insured_id

    else:
        insured_type_cls = Insured.get_class(insured_type)
        insured = get_object_or_404(insured_type_cls,
                                    pk=insured_id)  # type: Insured
        policy = Policy(insured=insured)
        policy.type = policy_type
        if prolong_id:
            policy.prolong_parent = get_object_or_404(Policy, pk=prolong_id)

    form_cls = PolicyForm.get_class(policy)
    form = form_cls(instance=policy)

    if request.method == 'POST':
        form = form_cls(request.POST)
        if form.is_valid():
            # policy = None
            # # Use already existed policy (imported ones)
            # if ('series' in form.cleaned_data) and \
            #    ('number' in form.cleaned_data) and \
            #    ('act' in form.cleaned_data):
            #         policy = Policy.objects.\
            #             filter(series=form.cleaned_data['series']).\
            #             filter(number=form.cleaned_data['number']).\
            #             filter(act=form.cleaned_data['act']).\
            #             first()
            #         if policy:
            #             del form.cleaned_data['series']
            #             del form.cleaned_data['number']
            #             del form.cleaned_data['act']
            #
            # # If there is not already existed one -> create new policy
            # if not policy:
            #     policy = Policy()
            # if hasattr(policy, 'company'):
            # if hasattr(policy, 'type'):
            #     form.cleaned_data['type'] = policy.type
            # if hasattr(policy, 'id') and policy.id:
            #     del form.cleaned_data['number']
            form.cleaned_data['company'] = form.cleaned_data['company'] or policy.company

            try:
                invoice = policy.invoice
            except Invoice.DoesNotExist as e:
                invoice = Invoice()

            for field in form.cleaned_data:
                try:
                    base_field = form.base_fields.get(field)
                    invoice_field_name = base_field.widget.attrs.get('invoice_field')
                    if invoice_field_name:
                        setattr(invoice, invoice_field_name, form.cleaned_data[field])
                    else:
                        setattr(policy, field, form.cleaned_data[field])
                except Exception as e:
                    pass

            policy.date_issued = datetime.now()
            if not hasattr(policy, 'branch'):
                policy.branch = request.user.branch
            # policy.company = get_object_or_404(InsuranceCompany, pk=1)
            policy.status = Policy.STATUS_ISSUED
            policy.insured = insured
            policy.save()

            if invoice.is_non_empty():
                invoice.policy = policy
                invoice.date_issued = invoice.date_issued or datetime.now()
                invoice.save()

            return redirect('policies:edit', policy_id=policy.id)

    if hasattr(policy, 'invoice'):
        form.initial['invoice_date_issued'] = policy.invoice.date_issued
        form.initial['invoice_number'] = policy.invoice.number
        form.initial['invoice_purpose'] = policy.invoice.purpose

    ctx['form'] = form
    ctx['policy'] = policy
    ctx['insured_type'] = insured_type
    ctx['insured_id'] = insured_id

    tpl = form.form_template
    return render(request, tpl, ctx)


# def policy_issue_vehicle(request, insured_type=None, insured_id=None, policy_id=None):
#     ctx = {}
#
#     insured = None
#     if policy_id:
#         policy = get_object_or_404(Policy, pk=policy_id)  # type: Policy
#     else:
#         insured_type_cls = Insured.get_class(insured_type)
#         insured = get_object_or_404(insured_type_cls,
#                                     pk=insured_id)  # type: Insured
#         policy = Policy(insured=insured)
#
#     form_cls = PolicyForm.get_class(policy.insured.policy_form())
#     form = form_cls(instance=policy)
#
#     if request.method == 'POST':
#         form = form_cls(request.POST)
#         if form.is_valid():
#             # policy = None
#             # # Use already existed policy (imported ones)
#             # if ('series' in form.cleaned_data) and \
#             #    ('number' in form.cleaned_data) and \
#             #    ('act' in form.cleaned_data):
#             #         policy = Policy.objects.\
#             #             filter(series=form.cleaned_data['series']).\
#             #             filter(number=form.cleaned_data['number']).\
#             #             filter(act=form.cleaned_data['act']).\
#             #             first()
#             #         if policy:
#             #             del form.cleaned_data['series']
#             #             del form.cleaned_data['number']
#             #             del form.cleaned_data['act']
#             #
#             # # If there is not already existed one -> create new policy
#             # if not policy:
#             #     policy = Policy()
#             if hasattr(policy, 'company'):
#                 form.cleaned_data['company'] = policy.company
#             if hasattr(policy, 'type'):
#                 form.cleaned_data['type'] = policy.type
#             if hasattr(policy, 'id') and policy.id:
#                 del form.cleaned_data['number']
#
#             for field in form.cleaned_data:
#                 setattr(policy, field, form.cleaned_data[field])
#             policy.date_issued = datetime.now()
#             # policy.company = get_object_or_404(InsuranceCompany, pk=1)
#             policy.status = Policy.STATUS_ISSUED
#             policy.insured = insured
#             policy.save()
#
#             return redirect('policies:edit', policy_id=policy.id)
#
#     ctx['form'] = form
#     ctx['policy'] = policy
#     tpl = form.form_template
#     return render(request, tpl, ctx)


# class PolicyIssue(View):
#     form_class = PolicyForm
#     template_name = 'policies/edit.html'
#
#     def get(self, request, insured_type=None, insured_id=None,
#             policy_id=None, delete=False):
#         ctx = {}
#
#         policy = None
#         if policy_id:
#             policy = get_object_or_404(Policy, id=policy_id)
#         else:
#             if insured_type and insured_id:
#                 insured_class = Insured.get_class(insured_type)
#                 insured = get_object_or_404(insured_class,
#                                             pk=insured_id)  # type: Insured
#
#                 policy = Policy(client=insured.client, insured=insured)
#
#         form_cls = PolicyForm.get_form_class_by_policy(policy)
#         form = form_cls(instance=policy)
#
#         # ctx['policy'] = policy
#         ctx['form'] = form
#         tpl = form.form_template
#         return render(request, tpl, ctx)
#
#     def post(self, request, insured_type=None, insured_id=None, policy_id=None):
#         ctx = {}
#
#         if policy_id:
#             policy = get_object_or_404(Policy, id=policy_id)  # type: Policy
#             insured = policy.insured
#         else:
#             insured_class = Insured.get_class(insured_type)
#             insured = get_object_or_404(insured_class,
#                                         pk=insured_id)  # type: Insured
#             policy = Policy(client=insured.client, insured=insured)
#
#         form_cls = PolicyForm.get_form_class_by_policy(policy)
#
#         form = form_cls(instance=policy)
#         if request.method == 'POST':
#             form = form_cls(request.POST)
#             if form.is_valid():
#                 if form.cleaned_data['number']:
#                     policy = get_object_or_404(Policy, id=form.cleaned_data['number'])
#                     del form.cleaned_data['number']
#                 else:
#                     policy = Policy()
#
#                 form.cleaned_data['company'] = policy.company or form.cleaned_data[
#                     'company']
#                 form.cleaned_data['type'] = policy.type or form.cleaned_data['type']
#                 for field in form.cleaned_data:
#                     setattr(policy, field, form.cleaned_data[field])
#                 policy.date_issued = datetime.now()
#                 # policy.company = get_object_or_404(InsuranceCompany, pk=1)
#                 policy.status = Policy.STATUS_ISSUED
#                 policy.insured = insured
#                 policy.save()
#
#                 return redirect('policies:edit', policy_id=policy.id)
#
#         ctx['form'] = form
#         tpl = form.form_template
#         return render(request, tpl, ctx)


@method_decorator(login_required, name='dispatch')
class InsuredEdit(View):
    template_name = 'clients/insured/edit.html'

    def get(self, request, client_id, insured_type, insured_id=None):
        ctx = {}
        insured = Insured.get_or_create(insured_type,
                                        insured_id)  # type: Insured

        form_cls = InsuredForm.get_class(insured.form())
        form = form_cls(instance=insured)

        ctx['insured'] = insured
        ctx['form'] = form
        return render(request, self.template_name, ctx)

    def post(self, request, client_id, insured_type=None, insured_id=None,
             delete=False):
        ctx = {}
        insured = Insured.get_or_create(insured_type,
                                        insured_id)  # type: Insured

        if delete:
            insured.delete()
            return redirect('clients:edit', client_id=client_id)

        form_cls = InsuredForm.get_class(insured.form())
        form = form_cls(request.POST)

        if form.is_valid():
            for field in form.cleaned_data:
                setattr(insured, field, form.cleaned_data[field])
            insured.client = get_object_or_404(Client, pk=client_id)
            insured.save()

            return redirect('clients:insured:edit',
                            client_id=client_id,
                            insured_type=insured_type,
                            insured_id=insured.id)

        ctx['form'] = form
        return render(request, self.template_name, ctx)


@permission_required('insurance_register_app.add_policiesimportact')
def policies_import(request):
    ctx = {}
    user = request.user

    initial = dict(
        branch=Branch.objects.first(),
        company=InsuranceCompany.objects.first(),
        type=Policy.TYPE_OSAGO,
    )
    form = PoliciesImportForm(initial=initial)
    if request.method == 'POST':
        form = PoliciesImportForm(request.POST)
        if form.is_valid():
            act = PoliciesImportAct(**form.cleaned_data)
            act.save()
            ctx['message'] = _('Successfully added import act')
    ctx['policies_import_form'] = form
    acts = PoliciesImportAct.objects
    filtering_branch = user.filtering_branch
    if filtering_branch:
        acts = acts.filter(branch=filtering_branch)

    acts = acts.order_by('-edited_at').all()
    paginator = Paginator(acts, 35)
    page = request.GET.get('page')
    try:
        acts = paginator.page(page)
    except PageNotAnInteger:
        acts = paginator.page(1)
    except EmptyPage:
        acts = paginator.page(paginator.num_pages)

    ctx['policies_import_acts_list'] = acts
    ctx['page_obj'] = acts
    ctx['is_paginated'] = True

    return render(request, 'policies_import/index.html', ctx)


@permission_required('insurance_register_app.view_policies_import_acts')
def import_policies_act_view(request, act_id):
    ctx = {
        'errors': [],
    }
    user = request.user
    act = get_object_or_404(PoliciesImportAct,
                            id=act_id)  # type:PoliciesImportAct

    if not user.check_user_access(act.branch):
        return HttpResponseForbidden()

    ctx['act'] = act
    ctx['fields'] = PoliciesImportAct.PRINT_FIELDS
    ctx['title'] = _('View act')

    # TODO: добавить права и печать ответного акта
    print_perm = 'insurance_register_app.import_policies_act_print'
    if user.has_perm(print_perm) and (act.STATUS_IN_BRANCH == act.status):
        ctx['btn'] = {
            "url": reverse_lazy('policies_import:act:approve', kwargs={
                "act_id": act.id,
            }),
            "title": _('Confirm delivery'),
        }

    if act.STATUS_NEW == act.status:
        ctx['btn'] = {
            "url": reverse_lazy('policies_import:act:in_branch', kwargs={
                "act_id": act.id,
            }),
            "title": _('Confirm delivery to branch'),
        }

    return render(request, 'policies_import/act/view.html', ctx)


@permission_required('insurance_register_app.import_policies_act_print')
def import_policies_print_dispatch_act(request, act_id):
    ctx = {
        'errors': [],
    }
    act = get_object_or_404(PoliciesImportAct, id=act_id)
    if not request.user.check_user_access(act.branch):
        return HttpResponseForbidden()

    ctx['act'] = act
    ctx['fields'] = PoliciesImportAct.PRINT_FIELDS
    ctx['title'] = _('View dispatch act')
    ctx['act_title'] = _('Dispatch act')
    return render(request, 'policies_import/act/dispatch_print.html', ctx)


@login_required
def import_policies_print_accept_act(request, act_id):
    ctx = {
        'errors': [],
    }
    act = get_object_or_404(PoliciesImportAct,
                            id=act_id)  # type: PoliciesImportAct
    if not act.recipient == request.user:
        return HttpResponseForbidden()

    ctx['act'] = act
    ctx['fields'] = PoliciesImportAct.PRINT_FIELDS
    ctx['title'] = _('View accept act')
    ctx['act_title'] = _('Accept act')
    return render(request, 'policies_import/act/accept_print.html', ctx)


@permission_required('insurance_register_app.import_policies_act_print')
def import_policies_act_confirm_delivery(request, act_id):
    act = get_object_or_404(PoliciesImportAct, id=act_id)

    act.make_policies()
    act.status = PoliciesImportAct.STATUS_CONF_DELV
    act.save()

    return redirect('policies_import:index')


@permission_required('insurance_register_app.import_policies_act_in_branch')
def import_policies_act_in_branch(request, act_id):
    act = get_object_or_404(PoliciesImportAct,
                            id=act_id)  # type: PoliciesImportAct
    branch = request.user.filtering_branch
    if branch and act.branch != branch:
        return HttpResponseForbidden()

    act.status = PoliciesImportAct.STATUS_IN_BRANCH
    act.save()

    return redirect('policies_import:index')


@permission_required('insurance_register_app.delete_policiesimportact')
def import_policies_act_delete(request, act_id):
    act = get_object_or_404(PoliciesImportAct, id=act_id)
    act.delete()

    return redirect('policies_import:index')


@method_decorator(login_required, name='dispatch')
class ClientListView(ListView):
    model = Client
    paginate_by = 20
    template_name = 'clients/list.html'
    context_object_name = 'clients'

    def get_queryset(self):
        clients = Client.objects
        form = ClientListForm(self.request.GET)
        # branch = self.request.user.filtering_branch
        # if branch:
        #     clients = clients.filter(branch=branch)
        if form.is_valid():
            if 'name' in form.cleaned_data and form.cleaned_data['name']:
                name_str = form.cleaned_data['name']
                clients = clients.annotate(name_fields=Concat('first_name',
                                                              'last_name',
                                                              'patronymic_name',
                                                              'title',
                                                              'tax_number'))
                for piece in name_str.split(' '):
                    clients = clients.filter(name_fields__icontains=piece)
                del form.cleaned_data['name']

            if 'phone' in form.cleaned_data and form.cleaned_data['phone']:
                phone_str = form.cleaned_data['phone']
                clients = clients.filter(phone__icontains=phone_str)
                del form.cleaned_data['phone']

            # if branch:
            #     del form.cleaned_data['branch']

            for field_name in form.cleaned_data:
                field_value = form.cleaned_data[field_name]
                if field_value:
                    form_field = form.fields[field_name]
                    dict = form_field.get_filter_dict(field_value)
                    q_object = Q(**dict)
                    clients = clients.filter(q_object)

        q = clients.all()
        from django.db import connection
        return q

    def get_context_data(self, **kwargs):
        context_data = super(ClientListView, self).get_context_data(**kwargs)
        context_data['client_types'] = Client.TYPES
        context_data['form'] = ClientListForm(self.request.GET)

        return context_data


@login_required
def client_edit(request, client_id):
    ctx = {}

    client = None  # type: Client
    if client_id:
        client = get_object_or_404(Client, id=client_id)
        # if not request.user.check_user_access(client.branch):
        #     return HttpResponseForbidden()

    if Client.TYPE_PERSON == client.type:
        form_cls = ClientPersonForm
        form_title = _('Edit client')
        title = _('Edit client %s') % client.full_name
    else:
        form_cls = ClientOrganizationForm
        form_title = _('Edit organization')
        title = _('Edit organization %s') % client.full_name

    form = form_cls(instance=client)

    similar_clients = []
    if request.method == 'POST':
        form = form_cls(request.POST)
        if form.is_valid():
            if not client:
                client = Client()
            similar_clients = Client.get_similar(form.cleaned_data)
            for attr, value in form.cleaned_data.items():
                setattr(client, attr, value)
            client.branch = request.user.branch
            client.save()

            # return redirect('clients:edit', client_id=client.id)

    ctx['form'] = form
    ctx['form_title'] = form_title
    ctx['client'] = client
    if len(similar_clients) > Client.MAX_SIMILAR_CLIENTS_COUNT:
        form.add_error(NON_FIELD_ERRORS, _('Found %d similar clients. You entered too few data?') % len(similar_clients))
        similar_clients = []
    ctx['similar_clients'] = similar_clients
    ctx['title'] = title

    return render(request, 'clients/edit.html', ctx)


@method_decorator(login_required, name='dispatch')
class ClientDelete(DeleteView):
    model = Client
    template_name = 'clients/delete.html'
    success_url = reverse_lazy('clients:list')
    pk_url_kwarg = 'client_id'

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()  # type: Client
        # if not request.user.check_user_access(self.object.branch):
        #     return HttpResponseForbidden()

        return super(ClientDelete, self).delete(request, *args, **kwargs)


@login_required
def client_add(request, client_type):
    client = Client(branch=request.user.branch, type=client_type)
    client.save()

    return redirect('clients:edit', client_id=client.id)


@login_required()
def api_policies_list(request):
    get_params = request.GET.copy()

    policies = Policy.objects.filter(status=Policy.STATUS_NEW)
    branch = request.user.filtering_branch
    if branch:
        policies = policies.filter(branch=branch)
    if branch in get_params:
        del get_params['branch']

    q_objects_and = Q()

    for param in get_params:
        q_objects_or = Q()
        filt = param + '__icontains'
        for param_value_part in get_params.get(param).split():
            dict = {filt: param_value_part}
            q_objects_or |= Q(**dict)

        q_objects_and &= q_objects_or

    policies = policies.filter(q_objects_and)

    policies = policies.values('id', 'series', 'number').all()
    policies = list(policies)
    response = {
        'data': policies,
        'count': len(policies),
    }

    return JsonResponse(response)


@login_required()
def api_unseen_important_policies_list(request):
    last_datetime = request.user.importants_last_reading_time or datetime.now()
    policies = Policy.objects.filter(important=True)\
                             .filter(edited_at__gt=last_datetime)
    branch = request.user.filtering_branch
    if branch:
        policies = policies.filter(branch=branch)
    count = policies.count()

    response = {
        'data': {
            'count': count,
            'datetime': last_datetime,
            'datetime_str': last_datetime.strftime('%d.%m.%Y %H:%M:%S'),
        },
        'status': 'ok',
    }

    return JsonResponse(response)


@login_required()
def api_policy_get(request, policy_id):
    policies = Policy.objects.filter(status=Policy.STATUS_NEW)

    if request.user.filtering_branch:
        policies = policies.filter(branch=request.user.filtering_branch)

    policies = policies.filter(pk=policy_id)

    policy = policies.values('id', 'series', 'number', 'company',
                             'type').first()
    response = {
        'data': policy,
        'count': 1,
    }

    return JsonResponse(response)


@require_POST
@login_required()
def api_policy_edit(request, policy_id):
    policy = get_object_or_404(Policy, pk=policy_id)  # type: Policy

    # if not request.user.check_user_access(policy.branch):
    #     return HttpResponseForbidden()

    if 'field' in request.POST and \
            hasattr(policy, request.POST['field']) and \
            'value' in request.POST:
        setattr(policy, request.POST['field'], request.POST['value'])
        policy.save()

    response = {
        'status': 'ok',
    }

    return JsonResponse(response)


@login_required()
def api_recipients_list(request):
    users = User.objects

    if request.GET.get('branch_id'):
        users = users.filter(branch=request.GET.get('branch_id'))

    users = users.values('id', 'first_name', 'last_name', 'branch_id').all()
    users = list(users)
    response = {
        'data': users,
        'count': len(users),
    }

    return JsonResponse(response)


@login_required()
def api_utilization_acts_list(request):
    acts = UtilizationAct.objects.order_by('-edited_at')
    if request.body:
        post_data = json.loads(request.body)
        if 'act_id' in post_data and post_data['act_id']:
            acts = acts.filter(pk__icontains=post_data['act_id'])
        if 'exclude' in post_data and post_data['exclude']:
            acts = acts.exclude(pk__in=list(post_data['exclude'].values()))

    acts = acts.values('id').all()[:15]
    acts = list(acts)
    response = {
        'data': acts,
        'count': len(acts),
    }

    return JsonResponse(response)


@method_decorator(login_required, name='dispatch')
class PaperEdit(View):
    form_class = PaperForm
    template_name = 'clients/papers/edit.html'

    def get(self, request, client_id, paper_id=None, delete=False):
        ctx = {}

        if paper_id:
            paper = Paper.objects.get(id=paper_id)

            # Remove current paper
            if delete:
                paper.delete()
                return redirect('clients:edit', client_id=client_id)
        else:
            paper = Paper()
            paper.client = Client.objects.get(id=client_id)
            paper.type = request.GET.get('type') or Paper.TYPE_PASSPORT

        form = self.form_class(instance=paper)

        ctx['paper'] = paper
        ctx['form'] = form
        return render(request, self.template_name, ctx)

    def post(self, request, client_id, paper_id=None):
        ctx = {}

        form = self.form_class(request.POST)
        if paper_id:
            paper = Paper.objects.get(id=paper_id)
            if form.is_valid():
                for field in form.cleaned_data:
                    setattr(paper, field, form.cleaned_data[field])
                paper.save()
        else:
            if form.is_valid():
                paper = form.save()
                return redirect('clients:papers:edit', client_id=client_id,
                                paper_id=paper.id)

        ctx['form'] = form

        return render(request, self.template_name, ctx)


@login_required
def report(request):
    ctx = {}
    # TODO: grants and access

    policies_list = Policy.objects
    form = PoliciesReportForm(request.GET)
    if form.is_valid():

        prolonged = form.cleaned_data['prolonged']
        print(prolonged)
        if '' != prolonged:
            prolonged_bool = bool(int(prolonged))
            prolonged_filter = policies_list.values('id').\
                filter(prolong_parent_id__isnull=False)
            if prolonged_bool:
                policies_list = policies_list.filter(id__in=prolonged_filter)
            else:
                policies_list = policies_list.exclude(id__in=prolonged_filter)

        del form.cleaned_data['prolonged']

        for field_name in form.cleaned_data:
            field_value = form.cleaned_data[field_name]
            if field_value:
                form_field = form.fields[field_name]
                dict = form_field.get_filter_dict(field_value)
                q_object = Q(**dict)
                policies_list = policies_list.filter(q_object)

    policies_list = policies_list.all()
    paginator = Paginator(policies_list, 25)  # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        policies = paginator.page(page)
    except PageNotAnInteger:
        policies = paginator.page(1)
    except EmptyPage:
        policies = paginator.page(paginator.num_pages)

    ctx['policies'] = policies
    ctx['form'] = form
    ctx['page_obj'] = policies
    ctx['is_paginated'] = True
    ctx['total'] = policies_list.aggregate(total=Sum('cost'))['total']

    return render(request, 'report.html', ctx)

@login_required
def invoices_report(request):
    ctx = {}

    invoices_list = Invoice.objects
    form = InvoicesReportForm(request.GET)
    if form.is_valid():
        if 'name' in form.cleaned_data and form.cleaned_data['name']:
            name_str = form.cleaned_data['name']
            invoices_list = invoices_list.annotate(name_fields=Concat(
                'policy__insured__client_first_name',
                'policy__insured__client_last_name',
                'policy__insured__client_patronymic_name',
                'policy__insured__client_title',
                'policy__insured__client_tax_number',
            ))
            for piece in name_str.split(' '):
                invoices_list = invoices_list.filter(name_fields__icontains=piece)
            del form.cleaned_data['name']

        for field_name in form.cleaned_data:
            field_value = form.cleaned_data[field_name]
            if field_value:
                form_field = form.fields[field_name]
                dict = form_field.get_filter_dict(field_value)
                q_object = Q(**dict)
                invoices_list = invoices_list.filter(q_object)

    invoices_list = invoices_list.all()
    paginator = Paginator(invoices_list, 25)
    page = request.GET.get('page')
    try:
        invoices = paginator.page(page)
    except PageNotAnInteger:
        invoices = paginator.page(1)
    except EmptyPage:
        invoices = paginator.page(paginator.num_pages)

    ctx['invoices'] = invoices
    ctx['form'] = form
    ctx['page_obj'] = invoices
    ctx['is_paginated'] = True
    # print(invoices_list.query)

    return render(request, 'invoices_report.html', ctx)


@method_decorator(login_required, name='dispatch')
class ExpensesListView(ListView):
    model = Expense
    paginate_by = 22
    ordering = '-edited_at'
    context_object_name = 'expenses'
    template_name = 'expenses/list.html'

    def get_queryset(self):
        expenses = super(ExpensesListView, self).get_queryset()

        form = ExpensesListForm(self.request.GET)
        if form.is_valid():
            if 'visibility' in form.cleaned_data:
                visibility = form.cleaned_data['visibility']
                if visibility:
                    visibility = int(visibility)

                if visibility == -1:
                    expenses = expenses.filter(hidden=0)
                if visibility == 1:
                    expenses = expenses.filter(hidden=1)
                del form.cleaned_data['visibility']

            for field_name in form.cleaned_data:
                field_value = form.cleaned_data[field_name]
                if field_value:
                    form_field = form.fields[field_name]
                    dict = form_field.get_filter_dict(field_value)
                    q_object = Q(**dict)
                    expenses = expenses.filter(q_object)
        branch = self.request.user.filtering_branch
        if branch:
            expenses = expenses.filter(branch=branch)

        perm = 'insurance_register_app.create_hidden_expense'
        if not self.request.user.has_perm(perm):
            expenses = expenses.filter(hidden=False)

        return expenses.all()

    def get_context_data(self, **kwargs):
        context_data = super(ExpensesListView, self).get_context_data(**kwargs)
        form = ExpensesListForm(self.request.GET)

        context_data['form'] = form
        context_data['cost'] = sum([e.cost for e in self.object_list])

        return context_data


@method_decorator(login_required, name='dispatch')
class ExpenseCreate(CreateView):
    model = Expense
    template_name = 'expenses/edit.html'
    form_class = ExpenseEditForm

    def get_initial(self):
        initial = super(ExpenseCreate, self).get_initial()
        initial = initial.copy()
        initial['branch'] = self.request.user.branch
        hidden_expense_perm = 'insurance_register_app.create_hidden_expense'
        initial['hidden'] = self.request.user.has_perm(hidden_expense_perm)

        return initial

    def form_valid(self, form):
        response = super(ExpenseCreate, self).form_valid(form)
        branch = self.request.user.filtering_branch
        if branch:
            self.object.branch = branch
            self.object.save()

        return response


@method_decorator(login_required, name='dispatch')
class ExpenseDelete(DeleteView):
    model = Expense
    template_name = 'expenses/delete.html'
    success_url = reverse_lazy('expenses:list')

    def get_object(self, queryset=None):
        expense = super(ExpenseDelete, self).get_object(queryset=queryset)
        if not expense.check_user_access(self.request.user):
            return HttpResponseForbidden()

        return expense

    def delete(self, request, *args, **kwargs):
        self.object = self.get_object()  # type: Expense
        if not request.user.check_user_access(self.object.branch):
            return HttpResponseForbidden()

        return super(ExpenseDelete, self).delete(request, *args, **kwargs)


@method_decorator(login_required, name='dispatch')
class ExpenseUpdate(UpdateView):
    model = Expense
    template_name = 'expenses/edit.html'
    form_class = ExpenseEditForm

    def get(self, request, *args, **kwargs):
        self.object = self.get_object()

        if not self.object.check_user_access(self.request.user):
            return HttpResponseForbidden()

        return super(ExpenseUpdate, self).get(request, *args, **kwargs)

    def form_valid(self, form):
        response = super(ExpenseUpdate, self).form_valid(form)
        branch = self.request.user.filtering_branch
        if branch:
            self.object.branch = branch

        if not self.object.check_user_access(self.request.user):
            return HttpResponseForbidden()

        self.object.save()

        return response


@login_required
def stats_view(request):
    form = StatisticsListForm(request.GET)
    conditions = []

    filtering_branch = request.user.filtering_branch
    if filtering_branch:
        conditions.append({'branch': filtering_branch})

    if form.is_valid():
        for field_name in form.cleaned_data:
            field_value = form.cleaned_data[field_name]
            if field_value:
                form_field = form.fields[field_name]
                filter_dict = form_field.get_filter_dict(field_value)
                conditions.append(filter_dict)

    stats = Statistics.get_data(conditions)
    stats['form'] = form
    stats['links_in_cells'] = True
    if 'date_from' in form.data: stats['date_from'] = form.data['date_from']
    if 'date_to' in form.data: stats['date_to'] = form.data['date_to']
    try:
        stats['host'] = settings.DOMAIN
    except AttributeError:
        stats['host'] = request.get_host()

    return render(request, 'statistics/stats.html', stats)


@login_required
def docs(request):
    ctx = dict()
    ctx['title'] = _('Documentation')

    return render(request, 'docs/index.html', ctx)


def handler403_view(request):
    ctx = dict()
    ctx['title'] = _('Access denied')
    ctx['backward_url'] = request.META.get('HTTP_REFERER')

    return render(request, '403.html', ctx, status=403)


def handler404_view(request):
    ctx = dict()
    ctx['title'] = _('Page not found')
    ctx['backward_url'] = request.META.get('HTTP_REFERER')

    return render(request, '404.html', ctx, status=404)


def handler500_view(request):
    ctx = dict()
    ctx['title'] = _('Error occured')
    ctx['backward_url'] = request.META.get('HTTP_REFERER')

    return render(request, '500.html', ctx, status=500)


class VehicleBrandListView(ListView):
    model = VehicleBrand
    paginate_by = 22
    ordering = 'title'
    context_object_name = 'brands'
    template_name = 'vehicle_brands/list.html'


class VehicleBrandCreate(CreateView):
    model = VehicleBrand
    template_name = 'vehicle_brands/edit.html'
    form_class = VehicleBrandEditForm


class VehicleBrandDelete(DeleteView):
    model = VehicleBrand
    template_name = 'vehicle_brands/delete.html'
    success_url = reverse_lazy('vehicle_brands:list')


class VehicleBrandUpdate(UpdateView):
    model = VehicleBrand
    template_name = 'vehicle_brands/edit.html'
    form_class = VehicleBrandEditForm

@login_required
def audit_logs(request):
    ctx = {}

    logs_list = AuditLog.objects.order_by('-edited_at').all()
    paginator = Paginator(logs_list, 25)  # Show 50 logs per page
    page = request.GET.get('page')
    try:
        logs = paginator.page(page)
    except PageNotAnInteger:
        logs = paginator.page(1)
    except EmptyPage:
        logs = paginator.page(paginator.num_pages)

    ctx['logs'] = logs
    ctx['is_paginated'] = True
    ctx['page_obj'] = logs

    return render(request, 'audit_logs/list.html', ctx)

@method_decorator(login_required, name='dispatch')
class InsuredAuditDetailView(DetailView):
    model = AuditedObjectMixin
    template_name = 'audit_logs/details.html'
    slug_url_kwarg = 'audited_type'
    pk_url_kwarg = 'audited_id'
    
    def get_object(self, queryset=None):
        audited_pk = self.kwargs.get(self.pk_url_kwarg)
        audited_cls = self.kwargs.get(self.slug_url_kwarg)
        if audited_pk is None and audited_cls is None:
            raise AttributeError("Generic detail view %s must be called with "
                                 "either an object pk or a slug."
                                 % self.__class__.__name__)

        print((audited_cls, audited_pk))

        return get_object_by_class_and_pk(audited_cls, audited_pk)

    def get_context_data(self, **kwargs):
        context = super(InsuredAuditDetailView, self).get_context_data(**kwargs)
        logs = self.object.logs.order_by('-change_number').all()
        context['logs'] = logs

        return context
