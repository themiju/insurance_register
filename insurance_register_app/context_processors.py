# -*- coding: utf-8 -*-
# from django.contrib.auth.context_processors import auth
import json
import os
from django.conf import settings as django_settings
from django.core.urlresolvers import resolve
from insurance_register_project import settings


# from asiou.institute.models import ASIOU_Institute
# from asiou.user_settings.models import ASIOU_U_UserSettings
# from asiou.properties.models import ASIOU_P_ParamValues


# Возвращает массив с параметрами конфигурации
# для того чтобы использовать его в представлениях
# def project_settings(request):
#     proto = 'http'
#     if request.is_secure(): proto = 'https'
#
#     return {
#         'settings': settings,
#         'protocol': proto,
#     }


# Возвращает список ОУ для выбора из него контекстного,
# а так же контекстное ОУ на данный момент
# def context_institute(request):
#     from asiou.common.util import list_of
#     filter_parms = {
#         'context': True
#     }
#
#     # Если не суперпользователь, то проверим на принадлежность к МР и
# отфильтруем список ОУ
#     if request.user.is_authenticated() and not request.user.is_superuser:
#         profile = request.user.profile
#         mr_code = profile.mr
#         if mr_code:
#             filter_parms['subordination'] = mr_code
#
#         if request.user.username.find('@') > 0:
#             (login, ou_code) = request.user.username.split('@');
#             filter_parms['id'] = ou_code
#
#     context_institute = None
#     all_context_list = ASIOU_Institute.objects.filter(**filter_parms)
#     if len(all_context_list):
#         context_institute = all_context_list[0]
#
#     # Возьмем код ОУ из кукисов, только если он входит в список возможных
# для доступа
#     readable_ou_codes = list_of(all_context_list, 'id', True, True)
#     if request.COOKIES.has_key('current_institute'):
#         filter_parms['id'] = int(request.COOKIES['current_institute'])
#         if (filter_parms['id'] in readable_ou_codes):
#             current = ASIOU_Institute.objects.filter(**filter_parms)
#             if len(current):
#                 context_institute = current[0]
#
#     # Взять доп параметры для контекстного ОУ
#     i_add_params = {}
#     if context_institute != None:
#         i_param_values = ASIOU_P_ParamValues.objects.filter(
#             institute=context_institute.pk,
#             record_id=context_institute.pk,
#             object=context_institute.__class__.__name__
#         )
#
#         for param_value in i_param_values:
#             i_add_params[param_value.param.label] = param_value.value()
#
#     if context_institute:
#         context_institute.params = i_add_params
#
#     # Получить текущий учебный год по умолчанию
#     cdate = datetime.date.today()
#     edu_year = cdate.year - 1
#     if cdate.day >= settings.APP_EDU_YEAR_BORDER_DAY and \
#                     cdate.month >= settings.APP_EDU_YEAR_BORDER_MONTH:
#         edu_year = cdate.year
#
#     # Год на основе выбора пользователя
#     if request.COOKIES.has_key('edu_year'):
#         try:
#             edu_year = int(request.COOKIES['edu_year'])
#         except:
#             pass
#
#     return {
#         'institute_list': all_context_list,
#         'context_institute': context_institute,
#         'edu_year': edu_year,
#         'edu_year_end': edu_year + 1
#     }


# Возвращает пользовательские настройки отображения
# def context_user_settings(request):
#     user = auth(request)['user']
#     context_user_settings = None
#     all_context_list = ASIOU_U_UserSettings.objects.filter(user=user.id)
#
#     if len(all_context_list):
#         context_user_settings = all_context_list[0]
#
#     if not context_user_settings and user.is_authenticated():
#         context_user_settings = ASIOU_U_UserSettings(user=user, settings='')
#         context_user_settings.save()
#
#     return {
#         'context_user_settings': context_user_settings
#     }


# Получить список прав на пункт меню
# def _get_rights(role_val):
#     item_roles = role_val.split(',')
#     item_rights = {}
#     for role in item_roles:
#         split_role = role.split(':')
#         if len(split_role) > 1:
#             if not split_role[0] in item_rights:
#                 item_rights[split_role[0]] = []
#             if settings.APP_RIGHT_READ in split_role[1]:
#                 item_rights[split_role[0]].append(settings.APP_RIGHT_READ)
#             if settings.APP_RIGHT_WRITE in split_role[1]:
#                 item_rights[split_role[0]].append(settings.APP_RIGHT_WRITE)
#         else:
#             if not split_role[0] in item_rights:
#                 item_rights[split_role[0]] = []
#             item_rights[split_role[0]].append(settings.APP_RIGHT_READ)
#
#     item_rights['super'] = [settings.APP_RIGHT_READ, settings.APP_RIGHT_WRITE]
#     item_roles = item_rights.keys()
#     return (item_roles, item_rights)


# Возвращает в контекст данные для главного меню
# Само меню размещено в XML файле, путь к нему указан
# в файле конфигурации
def general_menu(request):
    user = request.user
    if not request.user.is_authenticated():
        return {}

    menu_path = os.path.join(os.path.dirname(__file__), 'menu.json')
    menu = ()
    if os.path.isfile(menu_path):
        with open(menu_path) as data_file:
            menu_json = json.load(data_file)
            menu = menu_json[user.group]

    current_url_name = resolve(request.path_info).view_name

    for group_item in menu:
        if current_url_name == group_item['url']:
            group_item['class'] = 'active'
        if 'items' in group_item:
            for child_item in group_item['items']:
                if current_url_name == child_item['url']:
                    group_item['class'] = 'active'
                    child_item['class'] = 'active'

    ctx = {
        'menu': menu,
        'locale': settings.LANGUAGE_CODE,
        'current_path': request.path,
        'debug': django_settings.DEBUG,
    }
    if user and hasattr(user, 'group'):
        ctx['user_group'] = user.group
    return ctx


def languages(request):
    curr_code = request.LANGUAGE_CODE
    next_code = request.LANGUAGE_CODE
    for lang in settings.LANGUAGES:
        if lang[0] != curr_code:
            next_code = lang[0]

    return {
        'next_lang_code': next_code,
    }
