#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime

import pytz
from django.core.management.base import BaseCommand, CommandError
from django.urls import reverse

from insurance_register_app.models import Statistics
from django.template.loader import render_to_string
from django.utils import translation
from django.core.mail import send_mail
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class Command(BaseCommand):
    help = 'Sends statistic'

    def add_arguments(self, parser):
        parser.add_argument('--day_of_stats', default='', help="Day to send statistics for (YYYY.MM.DD)")

    def handle(self, *args, **options):
        translation.activate(settings.LANGUAGE_CODE or 'en')

        date_from = date_to = datetime.datetime.now()
        if 'day_of_stats' in options and options['day_of_stats']:
                date_to = date_from = datetime.datetime.strptime(options['day_of_stats'], '%Y.%m.%d')

        date_from = date_from.replace(hour=0, minute=0, second=1, microsecond=0)
        date_to = date_to.replace(hour=23, minute=59, second=59, microsecond=0)
        date_from = pytz.timezone(settings.TIME_ZONE).localize(date_from,
                                                               is_dst=None)
        date_to = pytz.timezone(settings.TIME_ZONE).localize(date_to,
                                                             is_dst=None)

        conditions = [
            {'date_issued__gte': date_from},
            {'date_issued__lte': date_to},
        ]

        stats = Statistics.get_data(conditions)

        stats['url'] = 'http://%s/%s?%s' % (
            settings.DOMAIN,
            reverse('view_statistics'),
            '&'.join([
                'date_from={:%d.%m.%Y}'.format(date_from),
                'date_to={:%d.%m.%Y}'.format(date_to)
            ])
        )
        stats['hidden_is_visible'] = True
        stats['links_in_cells'] = False
        stats['date_from'] = '{:%d.%m.%Y}'.format(date_from)
        stats['date_to'] = '{:%d.%m.%Y}'.format(date_to)
        try:
            stats['host'] = settings.DOMAIN
        except AttributeError:
            stats['host'] = 'localhost'

        table = render_to_string('statistics/mail.html', stats)

        title = _('Statistics for %s') % '{:%d.%m.%Y}'.format(date_from)
        send_mail(
            title,
            table,
            settings.STATISTICS_SENDER_MAIL,
            settings.STATISTICS_MAILS,
            fail_silently=False, html_message=table
        )

        self.stdout.write(self.style.SUCCESS(title))
