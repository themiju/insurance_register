#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.core.management.base import BaseCommand

from insurance_register_app.models import PoliciesImportAct
from insurance_register_app.models import Policy


class Command(BaseCommand):
    help = 'Fill policies numbers from its import acts'

    def add_arguments(self, parser):
        parser.add_argument('act_id', nargs='*', type=int)

    def handle(self, *args, **options):
        acts = PoliciesImportAct.objects
        if options['act_id']:
            acts = acts.filter(pk__in=options['act_id'])
        acts = acts.all()

        for act in acts:  # type: PoliciesImportAct
            policies = act.policy_set.order_by('id').all()
            strlen = len(act.last_policy_number)
            cnt = 0
            first = int(act.first_policy_number.lstrip('0'))

            for policy in policies:  # type: Policy
                # Avoid OSAGO number protection
                setattr(policy, '__original_number', None)
                policy.number = str(first + cnt).zfill(strlen)
                policy.save()
                cnt += 1

            msg = 'Act %s\nFirst %s\nLast %s\nLen %s\nCnt %d' % (act,
                                                                 act.first_policy_number,
                                                                 act.last_policy_number,
                                                                 strlen,
                                                                 cnt)
            self.stdout.write(self.style.SUCCESS(msg))
