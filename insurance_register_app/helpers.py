#!/usr/bin/env python2
# -*- coding: utf-8 -*-


def check_key_exists(arr, keys):
    res = True
    for key in keys.split('.'):
        res = res and (key in arr)
        if res:
            arr = arr[key]

    return res


def array_get(arr, keys, default=None):
    if isinstance(keys, str):
        keys = keys.split('.')

    try:
        for key in keys:
            arr = arr.get(key)
        res = arr
    except AttributeError:
        res = default

    return res
