#!/usr/bin/env python
# -*- coding: utf-8 -*-




try:
    from functools import wraps
except ImportError:
    from django.utils.functional import wraps  # Python 2.4 fallback.

from django.utils.decorators import available_attrs
from django.template import RequestContext, Template, loader, \
    TemplateDoesNotExist
from django.http import HttpResponse
# from django.db import *
# from doctor_scanner.settings import APP_CURRENT_ROLE_CHOICES, APP_RIGHT_READ, \
#     APP_RIGHT_WRITE


def check_rights(view_func):
    def _wrapper_view_func(request, *args, **kwargs):
        c = RequestContext(request)
        rights = c['menu']

        # # Если супер пользователь то больше ничего не проверяем
        # if request.user.is_superuser:
        #     return func(request, *args, **kwargs)
        #
        # # Если роль не существует
        # if not request.user.profile.roles_exists():
        #     try:
        #         t = loader.get_template('role_not_exists.html')
        #     except TemplateDoesNotExist:
        #         t = Template('<h1>User role not found!</h1>')
        #
        #     c['roles'] = APP_CURRENT_ROLE_CHOICES
        #     return HttpResponse(t.render(c))
        #
        # # Если нет прав доступа на чтение
        # if not (rights and APP_RIGHT_READ in rights):
        #     try:
        #         t = loader.get_template('access_denied.html')
        #     except TemplateDoesNotExist:
        #         t = Template('<h1>Read access denied!</h1>')
        #
        #     return HttpResponse(t.render(c))
        #
        # # Если нет прав на запись и производится попытка отправить POST
        # if (request.method == 'POST' or max_right == APP_RIGHT_WRITE) \
        #         and not (APP_RIGHT_WRITE in rights):
        #     try:
        #         t = loader.get_template('access_denied_w.html')
        #     except TemplateDoesNotExist:
        #         t = Template('<h1>Write access denied!</h1>')
        #
        #     return HttpResponse(t.render(c))

        return view_func(request, *args, **kwargs)
    return _wrapper_view_func


def add_branch(view_func):
    def _wrapper_view_func(request, *args, **kwargs):
        perm = 'insurance_register_app.access_its_branch_only'
        if request.user.has_perm(perm):
            request.POST = request.POST.copy()
            request.POST['branch'] = request.user.branch

            request.GET = request.POST.copy()
            request.GET['branch'] = request.user.branch

        return view_func(request, *args, **kwargs)

    return _wrapper_view_func
