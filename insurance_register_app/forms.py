#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime, date
from django import forms
from django.core.exceptions import NON_FIELD_ERRORS
from django.forms.utils import ErrorList
from django.urls import reverse_lazy
from django.utils.dateparse import parse_datetime
from django.db.models import Q
from django.forms.widgets import Input, TextInput, DateInput

from .models import Branch, Policy, PoliciesImportAct, Client, Paper, User, \
    InsuranceType, InsuranceCompany, InsuredVehicle, InsuredRealEstate, \
    InsuredPerson, Expense, ExpenseType, VehicleBrand, PAYMENT_TYPES, PROLONGED_TYPES
from django.utils.translation import ugettext_lazy as _
import pytz
from django.conf import settings


class CustomNameInput(Input):
    def get_context(self, name, value, attrs):
        context = super(CustomInput, self).get_context(name, value, attrs)
        if context['widget']['attrs'].get('name') is not None:
            context['widget']['name'] = context['widget']['attrs']['name']

        return context


class CustomNameTextInput(TextInput, CustomNameInput):
    pass


class CustomNameDateInput(DateInput, CustomNameInput):
    pass


class PoliciesBulkAddForm(forms.Form):
    def __init__(self, *args, **kwargs):
        super(PoliciesBulkAddForm, self).__init__(*args, **kwargs)
        try:
            self.fields['branch'].initial = Branch.objects.first()
        except:
            pass

    series = forms.CharField(label=_('Policies series'),
                             widget=forms.TextInput(
                                 attrs={'class': 'form-control',
                                        'placeholder': _(
                                            'Policies series')})
                             )

    branch = forms.ModelChoiceField(queryset=Branch.objects,
                                    label=_('Branches'),
                                    widget=forms.Select(
                                        attrs={'class': 'form-control',
                                               'placeholder': _(
                                                   'Branches')})
                                    )

    ph = _('Numbers only')
    start_policy_number = forms.IntegerField(label=_('First policy number'),
                                             max_value=pow(10, 10),
                                             min_value=0,
                                             widget=forms.NumberInput(
                                                 attrs={'class': 'form-control',
                                                        'placeholder': ph})
                                             )
    finish_policy_number = forms.IntegerField(label=_('Last policy number'),
                                              max_value=pow(10, 10),
                                              min_value=0,

                                              widget=forms.NumberInput(
                                                  attrs={
                                                      'class': 'form-control',
                                                      'placeholder': ph})
                                              )

    def clean(self):
        cleaned_data = super(PoliciesBulkAddForm, self).clean()
        if (cleaned_data['start_policy_number'] >
                cleaned_data['finish_policy_number']):
            self.add_error('start_policy_number',
                           _('Start number have to be less than last one'))
        else:
            return cleaned_data


ph = _('Numbers only')


class PoliciesImportForm(forms.ModelForm):
    def clean(self):
        cleaned_data = super(PoliciesImportForm, self).clean()
        first_raw = cleaned_data['first_policy_number']
        last_raw = cleaned_data['last_policy_number']
        if len(first_raw) != len(last_raw):
            raise forms.ValidationError({
                'first_policy_number': forms.ValidationError(
                    _(
                        'Policies numbers have different '
                        'lengths: %(first)s and %(last)s'),
                    code='incorrect_numbers_length',
                    params={'first': first_raw, 'last': last_raw, }
                ),
            })

        first = int(first_raw.lstrip('0'))
        last = int(last_raw.lstrip('0'))
        if first > last:
            raise forms.ValidationError({
                'first_policy_number': forms.ValidationError(
                    _('First policy (%(first)s) can not have '
                      'larger number then last one (%(last)s)'),
                    code='incorrect_numbers_first_is_bigger',
                    params={'first': first_raw, 'last': last_raw, },
                ),
            })

        return cleaned_data

    class Meta:
        model = PoliciesImportAct
        fields = ['series', 'first_policy_number', 'last_policy_number',
                  'branch', 'recipient', 'company', 'type', 'comment']

        widgets = {
            'first_policy_number': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': ph}),
            'last_policy_number': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': ph}),
            'series': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': _('Series')}),
            'branch': forms.Select(
                attrs={'class': 'form-control', 'placeholder': _('Branch')}),
            'recipient': forms.Select(
                attrs={'class': 'form-control', 'placeholder': _('Recipient'),
                       'id': 'id_recipient_select2'
                       }),
            'company': forms.Select(
                attrs={'class': 'form-control', 'placeholder': _('Company')}),
            'type': forms.Select(
                attrs={'class': 'form-control', 'placeholder': _('Type')}),
            'comment': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': _('Comment')}),
        }


class PolicyCorruptedForm(forms.Form):
    agree = forms.BooleanField(initial=True, widget=forms.HiddenInput(),
                               required=False)


class PolicyCancelForm(forms.Form):
    agree = forms.BooleanField(initial=True, widget=forms.HiddenInput(),
                               required=False)


class HtmlErrorList(ErrorList):
    error_list_class = 'list-group'
    error_item_class = 'list-group-item list-group-item-danger'

    def __str__(self):
        return self.as_html()

    def __unicode__(self):
        return self.as_html()

    def as_html(self):
        if not self:
            return ''

        errors = '<div><ol class="%s">%s</ol></div>' % (
            self.error_list_class,
            ''.join(['<li class="%s">%s</li>' % (self.error_item_class, e) for e in self]),
        )

        return errors


class ClientForm(forms.ModelForm):

    # MAX_SIMILAR_CLIENTS_COUNT = 10

    # def clean(self):
    #     data = super(ClientForm, self).clean()
    #     clients = Client.objects
    #     for field in self.unique_fields:
    #         if field in data and data[field]:
    #             dict = {field + '__icontains': data[field]}
    #             q_object = Q(**dict)
    #             clients = clients.filter(q_object)
    #
    #     clients = clients.all()
    #     if len(clients):
    #         self._errors[NON_FIELD_ERRORS] = HtmlErrorList()
    #         if len(clients) > self.MAX_SIMILAR_CLIENTS_COUNT:
    #             raise forms.ValidationError(
    #                 _('Found %(count)d similar clients. You entered too few data?'),
    #                 code='max_similar_clients_count_overcome',
    #                 params={'count': len(clients), }
    #             )
    #
    #         errors = []
    #         for client in clients:
    #             errors.append(forms.ValidationError(
    #                 _('Found similar client: <a href="%(url)s">%(client)s</a>'),
    #                 code='found_similar_clients',
    #                 params={
    #                     'url': reverse_lazy('clients:edit',
    #                                         kwargs={'client_id': client.pk}),
    #                     'client': client.full_name,
    #                 }
    #             ))
    #
    #         raise forms.ValidationError(errors)
    #
    #     return data

    @property
    def unique_fields(self):
        return []

    class Meta:
        model = Client
        fields = []

        widgets = {}


class ClientPersonForm(ClientForm):
    @property
    def unique_fields(self):
        return ['last_name', 'first_name', 'patronymic_name', 'phone']

    class Meta:
        model = Client
        fields = [
            'last_name', 'first_name', 'patronymic_name', 'date_of_birth',
            'phone', 'comment', ]

        widgets = {
            'last_name': forms.TextInput(
                attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(
                attrs={'class': 'form-control'}),
            'patronymic_name': forms.TextInput(
                attrs={'class': 'form-control'}),
            'phone': forms.TextInput(
                attrs={'class': 'form-control'}),
            'date_of_birth': forms.DateInput(
                format='%d.%m.%Y',
                attrs={
                    'class': 'form-control datepicker-here',
                    # 'data-date-format': 'dd.mm.yyyy',
                }),
            'comment': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': _('Comment')}),
        }


class ClientOrganizationForm(ClientForm):
    @property
    def unique_fields(self):
        return ['title', 'tax_number', 'phone', ]

    class Meta:
        model = Client
        fields = ['title', 'tax_number', 'address_legal', 'address_actual',
                  'phone', 'comment', ]

        widgets = {
            'title': forms.TextInput(
                attrs={'class': 'form-control'}),
            'tax_number': forms.TextInput(
                attrs={'class': 'form-control'}),
            'address_legal': forms.TextInput(
                attrs={'class': 'form-control'}),
            'address_actual': forms.TextInput(
                attrs={'class': 'form-control'}),
            'phone': forms.TextInput(
                attrs={'class': 'form-control'}),
            'comment': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': _('Comment')}),
        }


class PolicyForm(forms.ModelForm):
    prolong_parent = forms.ModelChoiceField(queryset=Policy.objects.all(), required=False,
                                            widget=forms.HiddenInput())
    # region Invoice fields
    invoice_number = forms.CharField(label=_('Invoice number'), required=False,
                                     widget=forms.TextInput(
                                         attrs={'class': 'form-control invoice',
                                                'invoice_field': 'number',
                                                'placeholder': _(
                                                    'Invoice number')})
                                     )

    invoice_date_issued = forms.DateTimeField(label=_('Date of invoice'), required=False,
                                              input_formats=('%d.%m.%Y %H:%M',),
                                              widget=forms.DateInput(
                                                  format='%d.%m.%Y %H:%M',
                                                  attrs={
                                                      'class': 'form-control datepicker-here invoice',
                                                      'placeholder': _('Date of invoice'),
                                                      'invoice_field': 'date_issued',
                                                      'readonly': False,
                                                      'data-date-format': 'dd.mm.yyyy',
                                                      'data-timepicker': 'true',
                                                      'data-time-format': 'hh:ii',
                                                  }))

    invoice_purpose = forms.CharField(label=_('Invoice purpose'), required=False,
                                      widget=forms.Textarea(
                                          attrs={
                                              'class': 'form-control invoice',
                                              'invoice_field': 'purpose',
                                              'placeholder': _('Purpose')
                                          }))
    # endregion

    TYPES_POLICY_FORMS = {}

    form_template = 'policies/edit.html'

    # @classmethod
    # def get_subclasses(cls):
    #     return cls.__subclasses__()

    # @classmethod
    # def get_class_name(cls):
    #     return cls.__name__

    # @classmethod
    # def get_subclass(cls, class_name):
    #     res = None
    #     if class_name:
    #         res = cls.get_subclasses()[class_name]
    #     return res

    @classmethod
    def policy_type(cls):
        return None

    @classmethod
    def get_class(cls, policy):
        policy_form_class = cls
        classes = cls.__subclasses__()
        for cur_cls in classes:
            if policy.type == cur_cls.policy_type():
                policy_form_class = cur_cls

        return policy_form_class

    # @classmethod
    # def get_form_class_by_policy(cls, policy):
    #     form_class = cls
    #     if policy.insured and \
    #        policy.insured.type and \
    #        (policy.insured.type in cls.TYPES_POLICY_FORMS):
    #         form_class = cls.TYPES_POLICY_FORMS[policy.insured.type]
    #
    #     return form_class

    def __init__(self, *args, **kwargs):
        super(PolicyForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if field in self.instance.instant_save_fields:
                classes = 'instant_save'
                if 'class' in self.fields[field].widget.attrs:
                    classes += ' ' + self.fields[field].widget.attrs['class']
                self.fields[field].widget.attrs['class'] = classes
            else:
                self.fields[field].widget.attrs['readonly'] = self.instance.is_readonly

    # def clean_client(self):
    #     client = self.cleaned_data['client']
    #     if not client:
    #         self.add_error('client',
    #                        _('Required field'))
    #     else:
    #         return client

    def clean(self):
        cleaned_data = super(PolicyForm, self).clean()
        if self.instance.is_readonly:
            raise forms.ValidationError(_("Policy can't be updated or deleted"),
                                        code='policy_is_readonly')

        return cleaned_data

    class Meta:
        model = Policy
        fields = [
            # 'client',
            'company',
            'type',
            'series',
            'number',
            'date_issued',
            'cost',
            'payment_type',
            'receipt_number',
            'comment',
            'important',
            # 'act',
            'prolong_parent',
            'invoice_number',
            'invoice_date_issued',
            'invoice_purpose',
        ]

        widgets = {
            'company': forms.Select(
                attrs={'class': 'form-control'}),
            'type': forms.Select(
                attrs={
                    'class': 'form-control disabled',
                    'readonly': True,
                }),
            'series': forms.TextInput(
                attrs={'class': 'form-control'}),
            'number': forms.TextInput(
                attrs={'class': 'form-control'}),
            'date_issued': forms.DateInput(
                format='%d.%m.%Y %H:%M',
                attrs={
                    'class': 'form-control',
                    'readonly': True,
                }),
            'import_act': forms.TextInput(
                attrs={'class': 'form-control', 'readonly': True}),
            # 'client': forms.Select(
            #     attrs={'class': 'form-control disabled', 'readonly': True}),
            'receipt_number': forms.TextInput(
                attrs={'class': 'form-control', }),
            'comment': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': _('Comment')}),
            'important': forms.CheckboxInput(
                attrs={'class': 'checkbox'}),
            'cost': forms.NumberInput(
                attrs={'class': 'form-control cost-field', 'required': True}),
            'payment_type': forms.Select(
                attrs={
                    'class': 'form-control'
                }
            ),
            # 'prolong_parent': forms.HiddenInput(),
        }


class PolicyOsagoForm(PolicyForm):
    # number = forms.CharField(required=False, max_length=30, label=_('Number'))
    company = forms.ModelChoiceField(InsuranceCompany.objects, required=False,
                                     label=_('Insurance company'), widget=forms.Select(
                                         attrs={
                                             'class': 'form-control disabled',
                                             'readonly': True,
                                         }))

    # region Invoice fields
    invoice_number = forms.CharField(label=_('Invoice number'), required=False,
                                     widget=forms.TextInput(
                                         attrs={'class': 'form-control invoice',
                                                'invoice_field': 'number',
                                                'placeholder': _(
                                                    'Invoice number')})
                                     )

    invoice_date_issued = forms.DateTimeField(label=_('Date of invoice'), required=False,
                                              input_formats=('%d.%m.%Y %H:%M',),
                                              widget=forms.DateInput(
                                                  format='%d.%m.%Y %H:%M',
                                                  attrs={
                                                      'class': 'form-control datepicker-here invoice',
                                                      'placeholder': _('Date of invoice'),
                                                      'invoice_field': 'date_issued',
                                                      'readonly': False,
                                                      'data-date-format': 'dd.mm.yyyy',
                                                      'data-timepicker': 'true',
                                                      'data-time-format': 'hh:ii',
                                                  }))

    invoice_purpose = forms.CharField(label=_('Invoice purpose'), required=False,
                                      widget=forms.Textarea(
                                          attrs={
                                              'class': 'form-control invoice',
                                              'invoice_field': 'purpose',
                                              'placeholder': _('Purpose')
                                          }))
    # endregion

    form_template = 'policies/edit_vehicle.html'

    def __init__(self, *args, **kwargs):
        super(PolicyOsagoForm, self).__init__(*args, **kwargs)
        if self.instance.id:
            for field in self.fields:
                if field in self.instance.instant_save_fields:
                    classes = 'instant_save'
                    if 'class' in self.fields[field].widget.attrs:
                        classes += ' ' + self.fields[field].widget.attrs['class']
                    self.fields[field].widget.attrs['class'] = classes
                else:
                    self.fields[field].widget.attrs['readonly'] = True

    @classmethod
    def policy_type(cls):
        return Policy.TYPE_OSAGO

    class Meta:
        model = Policy
        fields = [
            # 'client',
            'number',
            'company',
            'type',
            # 'series',
            'date_issued',
            # 'act',
            'cost',
            'payment_type',
            'receipt_number',
            'comment',
            'important',
            'prolong_parent',
        ]

        widgets = {
            'company': forms.Select(
                attrs={'class': 'form-control disabled'}),
            'type': forms.Select(
                attrs={
                    'class': 'form-control disabled',
                    'readonly': True,
                }),
            'series': forms.TextInput(
                attrs={'class': 'form-control disabled', 'readonly': True}),
            'number': forms.Select(
                attrs={'class': 'form-control'}),
            'date_issued': forms.DateInput(
                format='%d.%m.%Y %H:%M',
                attrs={
                    'class': 'form-control',
                    'readonly': True,
                }),
            'import_act': forms.TextInput(
                attrs={'class': 'form-control', 'readonly': True}),
            # 'client': forms.HiddenInput(
            #     attrs={'class': 'form-control'}),
            'receipt_number': forms.TextInput(
                attrs={'class': 'form-control', }),
            'comment': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': _('Comment')}),
            'important': forms.CheckboxInput(
                attrs={'class': 'checkbox'}),
            'cost': forms.NumberInput(
                attrs={'class': 'form-control cost-field', 'required': True}),
            'payment_type': forms.Select(
                attrs={
                    'class': 'form-control',
                }
            ),
        }


class PolicyTicketForm(PolicyForm):
    # number = forms.CharField(required=False, max_length=30, label=_('Number'))
    company = forms.ModelChoiceField(InsuranceCompany.objects, required=False,
                                     label=_('Insurance company'), widget=forms.Select(
                                         attrs={
                                             'class': 'form-control disabled',
                                             'readonly': True,
                                         }))

    # region Invoice fields
    invoice_number = forms.CharField(label=_('Invoice number'), required=False,
                                     widget=forms.TextInput(
                                         attrs={'class': 'form-control invoice',
                                                'invoice_field': 'number',
                                                'placeholder': _(
                                                    'Invoice number')})
                                     )

    invoice_date_issued = forms.DateTimeField(label=_('Date of invoice'), required=False,
                                              input_formats=('%d.%m.%Y %H:%M',),
                                              widget=forms.DateInput(
                                                  format='%d.%m.%Y %H:%M',
                                                  attrs={
                                                      'class': 'form-control datepicker-here invoice',
                                                      'placeholder': _('Date of invoice'),
                                                      'invoice_field': 'date_issued',
                                                      'readonly': False,
                                                      'data-date-format': 'dd.mm.yyyy',
                                                      'data-timepicker': 'true',
                                                      'data-time-format': 'hh:ii',
                                                  }))

    invoice_purpose = forms.CharField(label=_('Invoice purpose'), required=False,
                                      widget=forms.Textarea(
                                          attrs={
                                              'class': 'form-control invoice',
                                              'invoice_field': 'purpose',
                                              'placeholder': _('Purpose')
                                          }))
    # endregion

    form_template = 'policies/edit_vehicle.html'

    def __init__(self, *args, **kwargs):
        super(PolicyTicketForm, self).__init__(*args, **kwargs)
        if self.instance.id:
            for field in self.fields:
                if field in self.instance.instant_save_fields:
                    classes = 'instant_save'
                    if 'class' in self.fields[field].widget.attrs:
                        classes += ' ' + self.fields[field].widget.attrs['class']
                    self.fields[field].widget.attrs['class'] = classes
                else:
                    self.fields[field].widget.attrs['readonly'] = True

    @classmethod
    def policy_type(cls):
        return Policy.TYPE_TICKET

    class Meta:
        model = Policy
        fields = [
            # 'client',
            'number',
            'company',
            'type',
            # 'series',
            'date_issued',
            # 'act',
            'cost',
            'payment_type',
            'receipt_number',
            'comment',
            'important',
            'prolong_parent',
        ]

        widgets = {
            'company': forms.Select(
                attrs={'class': 'form-control disabled'}),
            'type': forms.Select(
                attrs={
                    'class': 'form-control disabled',
                    'readonly': True,
                }),
            'series': forms.TextInput(
                attrs={'class': 'form-control disabled', 'readonly': True}),
            'number': forms.Select(
                attrs={'class': 'form-control'}),
            'date_issued': forms.DateInput(
                format='%d.%m.%Y %H:%M',
                attrs={
                    'class': 'form-control',
                    'readonly': True,
                }),
            'import_act': forms.TextInput(
                attrs={'class': 'form-control', 'readonly': True}),
            # 'client': forms.HiddenInput(
            #     attrs={'class': 'form-control'}),
            'receipt_number': forms.TextInput(
                attrs={'class': 'form-control', }),
            'comment': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': _('Comment')}),
            'important': forms.CheckboxInput(
                attrs={'class': 'checkbox'}),
            'cost': forms.NumberInput(
                attrs={'class': 'form-control cost-field', 'required': True}),
            'payment_type': forms.Select(
                attrs={
                    'class': 'form-control',
                }
            ),
        }


class PolicyDiagCardForm(PolicyForm):
    company = forms.ModelChoiceField(InsuranceCompany.objects,
                                     initial=InsuranceCompany.objects.filter(title="Техосмотр", ).first(),
                                     widget=forms.HiddenInput(
                                         attrs={
                                             'class': 'form-control disabled',
                                         }))

    # region Invoice fields
    invoice_number = forms.CharField(label=_('Invoice number'), required=False,
                                     widget=forms.TextInput(
                                         attrs={'class': 'form-control invoice',
                                                'invoice_field': 'number',
                                                'placeholder': _(
                                                    'Invoice number')})
                                     )

    invoice_date_issued = forms.DateTimeField(label=_('Date of invoice'), required=False,
                                              input_formats=('%d.%m.%Y %H:%M',),
                                              widget=forms.DateInput(
                                                  format='%d.%m.%Y %H:%M',
                                                  attrs={
                                                      'class': 'form-control datepicker-here invoice',
                                                      'placeholder': _('Date of invoice'),
                                                      'invoice_field': 'date_issued',
                                                      'readonly': False,
                                                      'data-date-format': 'dd.mm.yyyy',
                                                      'data-timepicker': 'true',
                                                      'data-time-format': 'hh:ii',
                                                  }))

    invoice_purpose = forms.CharField(label=_('Invoice purpose'), required=False,
                                      widget=forms.Textarea(
                                          attrs={
                                              'class': 'form-control invoice',
                                              'invoice_field': 'purpose',
                                              'placeholder': _('Purpose')
                                          }))
    # endregion

    form_template = 'policies/edit_diadcard.html'

    def __init__(self, *args, **kwargs):
        super(PolicyDiagCardForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if field in self.instance.instant_save_fields:
                classes = 'instant_save'
                if 'class' in self.fields[field].widget.attrs:
                    classes += ' ' + self.fields[field].widget.attrs['class']
                self.fields[field].widget.attrs['class'] = classes


    @classmethod
    def policy_type(cls):
        return Policy.TYPE_DIADCARD

    class Meta:
        model = Policy
        fields = [
            'number',
            'date_issued',
            'cost',
            'receipt_number',
            'comment',
            'important',
            'prolong_parent',
        ]

        hidden_fields = [
            'company',
        ]

        widgets = {
            'number': forms.TextInput(
                attrs={'class': 'form-control'}),
            'date_issued': forms.DateInput(
                format='%d.%m.%Y %H:%M',
                attrs={
                    'class': 'form-control',
                    'readonly': True,
                }),
            'receipt_number': forms.TextInput(
                attrs={'class': 'form-control', }),
            'comment': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': _('Comment')}),
            'important': forms.CheckboxInput(
                attrs={'class': 'checkbox'}),
            'cost': forms.NumberInput(
                attrs={'class': 'form-control cost-field', 'required': True}),
        }


class InsuredForm(forms.Form):
    TYPES_FORMS = {}

    @classmethod
    def get_class(cls, class_name):
        return globals()[class_name]


class InsuredVehicleForm(forms.ModelForm, InsuredForm):
    # brand = forms.CharField(label=_('Brand'), max_length=100)
    # model = forms.CharField(label=_('Model'), max_length=50)
    # tag = forms.CharField(label=_('Tag'), max_length=9)
    # client = forms.CharField(label=_('Model'), max_length=50)

    class Meta:
        model = InsuredVehicle
        fields = ['brand', 'tag', 'model',]
        widgets = {
            'brand': forms.Select(
                attrs={'class': 'form-control'}
            ),
            'model': forms.TextInput(
                attrs={'class': 'form-control'}
            ),
            'tag': forms.TextInput(
                attrs={'class': 'form-control'}
            ),
            'client': forms.Select(
                attrs={'class': 'form-control disabled', 'readonly': True}
            ),
        }
# InsuredForm.TYPES_FORMS[Insured.TYPE_VEHICLE] = InsuredVehicleForm


class InsuredRealestateForm(forms.ModelForm, InsuredForm):
    # address = forms.CharField(label=_('Address'), max_length=2000)

    class Meta:
        model = InsuredRealEstate
        fields = ['address', ]

        widgets = {
            'address': forms.TextInput(
                attrs={'class': 'form-control'}
            ),
            'client': forms.Select(
                attrs={'class': 'form-control disabled', 'readonly': True}
            ),
        }
# InsuredForm.TYPES_FORMS[Insured.TYPE_REALESTATE] = InsuredRealestateForm


class InsuredPersonForm(forms.ModelForm, InsuredForm):
    # person = forms.CharField(label=_('Full name'), max_length=200)

    class Meta:
        model = InsuredPerson
        fields = ['last_name', 'first_name', 'patronymic_name', ]

        widgets = {
            'last_name': forms.TextInput(
                attrs={'class': 'form-control'}),
            'first_name': forms.TextInput(
                attrs={'class': 'form-control'}),
            'patronymic_name': forms.TextInput(
                attrs={'class': 'form-control'}),

            'client': forms.Select(
                attrs={'class': 'form-control disabled', 'readonly': True}
            ),
        }
# InsuredForm.TYPES_FORMS[Insured.TYPE_PERSON] = InsuredPersonForm


class SearchFieldMixin(forms.Field):
    def __init__(self, compare='', field='', *args, **kwargs):
        super(SearchFieldMixin, self).__init__(*args, **kwargs)
        self.compare = compare
        self.field = field

    def get_compare_string(self):
        compare = self.field
        if self.compare:
            compare += '__' + self.compare

        return compare

    def get_filter_dict(self, value):
        return {self.get_compare_string(): self.format_value(value)}

    def format_value(self, value):
        return value


class SearchDateField(SearchFieldMixin, forms.DateField):
    def __init__(self, value_format='%Y-%m-%d 00:00:00', *args, **kwargs):
        super(SearchDateField, self).__init__(*args, **kwargs)
        self.value_format = value_format

    def format_value(self, dt):
        # Convert date to datetime-string with our self.value_format format
        dttm = parse_datetime(dt.strftime(self.value_format))
        dttm = pytz.timezone(settings.TIME_ZONE).localize(dttm, is_dst=None)

        return dttm


class SearchModelChoiceField(SearchFieldMixin, forms.ModelChoiceField):
    pass


class SearchCharField(SearchFieldMixin, forms.CharField):
    pass


class SearchChoiceField(SearchFieldMixin, forms.ChoiceField):
    def __init__(self, choices=(), empty_label=None, required=True, widget=None,
                 label=None,
                 initial=None, help_text=None, *args, **kwargs):
        # prepend an empty label if it exists (and field is not required!)
        if not required and empty_label is not None:
            choices = tuple([('', empty_label)] + list(choices))

        super(SearchChoiceField, self).__init__(choices=choices,
                                                required=required,
                                                widget=widget, label=label,
                                                initial=initial,
                                                help_text=help_text, *args,
                                                **kwargs)


class PoliciesReportForm(forms.Form):
    number = SearchCharField(label=_('Number'), required=False,
                             field='number',
                             compare='icontains',
                             widget=forms.TextInput(
                                 attrs={
                                     'class': 'form-control'
                                 })
                             )

    receipt_number = SearchCharField(label=_('Receipt number'), required=False,
                                     field='receipt_number',
                                     compare='icontains',
                                     widget=forms.TextInput(
                                         attrs={
                                             'class': 'form-control'
                                         })
                                     )

    date_from = SearchDateField(label=_('Date from'), compare='gte',
                                required=False,
                                field='date_issued', widget=forms.DateInput(
        format='%d.%m.%Y',
        attrs={
            'class': 'form-control datepicker-here',
            'data-auto-close': 1,
        }))

    date_to = SearchDateField(label=_('Date to'), compare='lte', required=False,
                              value_format='%Y-%m-%d 23:59:59',
                              field='date_issued', widget=forms.DateInput(
        format='%d.%m.%Y',
        attrs={
            'class': 'form-control datepicker-here',
            'data-auto-close': 1,
        }))

    type = SearchChoiceField(choices=Policy.TYPES,
                             required=False, empty_label='---------',
                             label=_('Type'), field='type',
                             widget=forms.Select(
                                 attrs={
                                     'class': 'form-control',
                                 }))

    branch = SearchModelChoiceField(queryset=Branch.objects.all(),
                                    required=False,
                                    label=_('Branch'), field='branch',
                                    widget=forms.Select(
                                        attrs={
                                            'class': 'form-control',
                                        }))

    company = SearchModelChoiceField(queryset=InsuranceCompany.objects.all(),
                                     required=False,
                                     label=_('Company'), field='company',
                                     widget=forms.Select(
                                         attrs={
                                             'class': 'form-control',
                                         }))

    status = SearchChoiceField(choices=Policy.STATUSES,
                               required=False, empty_label='---------',
                               label=_('Status'), field='status',
                               widget=forms.Select(
                                   attrs={
                                       'class': 'form-control',
                                   }))

    payment_type = SearchChoiceField(choices=PAYMENT_TYPES,
                                     required=False, empty_label='---------',
                                     label=_('Payment type'),
                                     field='payment_type',
                                     widget=forms.Select(
                                         attrs={
                                             'class': 'form-control',
                                         }))
    prolonged = SearchChoiceField(choices=[(0, _("Not prolonged")), (1, _("Prolonged"))],
                                  required=False, empty_label='---------',
                                  label=_('Prolonged'),
                                  field='prolonged',
                                  widget=forms.Select(
                                      attrs={
                                          'class': 'form-control',
                                      }))


class InvoicesReportForm(forms.Form):
    number = SearchCharField(label=_('Number'), required=False,
                             field='number',
                             compare='icontains',
                             widget=forms.TextInput(
                                 attrs={
                                     'class': 'form-control'
                                 })
                             )

    # name = forms.CharField(label=_('Name, title or tax number'), required=False,
    #                        widget=forms.TextInput(
    #                            attrs={
    #                                'class': 'form-control'
    #                            })
    #                        )

    date_from = SearchDateField(label=_('Date from'), compare='gte', required=False,
                                field='date_issued', widget=forms.DateInput(
            format='%d.%m.%Y',
            attrs={
                'class': 'form-control datepicker-here',
                'data-auto-close': 1,
            }))

    date_to = SearchDateField(label=_('Date to'), compare='lte', required=False,
                              value_format='%Y-%m-%d 23:59:59',
                              field='date_issued', widget=forms.DateInput(
            format='%d.%m.%Y',
            attrs={
                'class': 'form-control datepicker-here',
                'data-auto-close': 1,
            }))

    purpose = SearchCharField(label=_('Purpose'), required=False,
                              field='purpose',
                              compare='icontains',
                              widget=forms.TextInput(
                                  attrs={
                                      'class': 'form-control'
                                  })
                              )

    cost_from = SearchCharField(label=_('Cost from'), required=False,
                                field='policy__cost',
                                compare='gte',
                                widget=forms.TextInput(
                                    attrs={
                                        'class': 'form-control'
                                    })
                                )

    cost_to = SearchCharField(label=_('Cost to'), required=False,
                              field='policy__cost',
                              compare='lte',
                              widget=forms.TextInput(
                                  attrs={
                                      'class': 'form-control'
                                  })
                              )


class PaperForm(forms.ModelForm):
    class Meta:
        model = Paper
        fields = [
            'client',
            'type',
            'series',
            'number',
            'date_issued',
            'issued_by',
            'comment',
        ]

        widgets = {
            'client': forms.Select(
                attrs={'class': 'form-control disabled', 'readonly': True}),
            'type': forms.Select(
                attrs={'class': 'form-control disabled', 'readonly': True}),
            'series': forms.TextInput(
                attrs={'class': 'form-control'}),
            'number': forms.TextInput(
                attrs={'class': 'form-control'}),
            'issued_by': forms.TextInput(
                attrs={'class': 'form-control'}),
            'date_issued': forms.DateInput(
                format='%d.%m.%Y',
                attrs={
                    'class': 'form-control datepicker-here',
                    # 'data-date-format': 'dd.mm.yyyy',
                }),
            'comment': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': _('Comment')}),
        }


class ExpenseEditForm(forms.ModelForm):
    class Meta:
        model = Expense
        fields = [
            'branch',
            'type',
            'cost',
            'hidden',
            'comment',
        ]

        widgets = {
            'branch': forms.Select(
                attrs={'class': 'form-control'}),
            'type': forms.Select(
                attrs={'class': 'form-control'}),
            "cost": forms.NumberInput(
                attrs={'class': 'form-control'}),
            "hidden": forms.HiddenInput(
                attrs={'class': 'checkbox'}),
            'comment': forms.Textarea(
                attrs={'class': 'form-control', 'placeholder': _('Comment')}),
        }


class VehicleBrandEditForm(forms.ModelForm):
    class Meta:
        model = VehicleBrand
        fields = [
            'title',
        ]

        widgets = {
            'title': forms.TextInput(
                attrs={'class': 'form-control', 'placeholder': _('Title')}),
        }


class ExpensesListForm(forms.Form):
    date_from = SearchDateField(label=_('Date from'), compare='gte', required=False,
                              value_format='%Y-%m-%d 00:00:00',
                              field='edited_at', widget=forms.DateInput(
            format='%d.%m.%Y',
            attrs={
                'class': 'form-control datepicker-here',
                'data-auto-close': 1,
            }))

    date_to = SearchDateField(label=_('Date to'), compare='lte', required=False,
                              value_format='%Y-%m-%d 23:59:59',
                              field='edited_at', widget=forms.DateInput(
            format='%d.%m.%Y',
            attrs={
                'class': 'form-control datepicker-here',
                'data-auto-close': 1,
            }))

    type = SearchModelChoiceField(queryset=ExpenseType.objects.all(),
                                  required=False,
                                  label=_('Type'), field='type',
                                  widget=forms.Select(
                                      attrs={
                                          'class': 'form-control',
                                      }))

    branch = SearchModelChoiceField(queryset=Branch.objects.all(),
                                    required=False,
                                    label=_('Branch'), field='branch',
                                    widget=forms.Select(
                                        attrs={
                                            'class': 'form-control',
                                        }))

    visibility = forms.ChoiceField(choices=[(-1, _('Regular')), (1, _('Hidden')), (0, _('All'))],
                                   required=False,
                                   label=_('Visibility'),
                                   widget=forms.Select(
                                       attrs={
                                           'class': 'form-control',
                                       })
                                   )


class StatisticsListForm(forms.Form):
    date_from = SearchDateField(label=_('Date from'), compare='gte',
                                required=False,
                                value_format='%Y-%m-%d 00:00:00',
                                field='date_issued', widget=forms.DateInput(
            format='%d.%m.%Y',
            attrs={
                'class': 'form-control datepicker-here',
                'data-auto-close': 1,
            }))

    date_to = SearchDateField(label=_('Date to'), compare='lte', required=False,
                              value_format='%Y-%m-%d 23:59:59',
                              field='date_issued', widget=forms.DateInput(
            format='%d.%m.%Y',
            attrs={
                'class': 'form-control datepicker-here',
                'data-auto-close': 1,
            }))

    def clean(self):
        cleaned_data = super(StatisticsListForm, self).clean()
        date_from = date.min
        date_to = date.max
        if ('date_from' in cleaned_data) and (cleaned_data['date_from']):
            date_from = cleaned_data['date_from']

        if ('date_to' in cleaned_data) and (cleaned_data['date_to']):
            date_to = cleaned_data['date_to']

        if date_from > date_to:
            self.add_error('date_from', _('First date more than last one'))
        else:
            return cleaned_data


class ClientListForm(forms.Form):
    name = forms.CharField(label=_('Name, title or tax number'), required=False,
                           widget=forms.TextInput(
                               attrs={
                                   'class': 'form-control'
                               })
                           )

    phone = forms.CharField(label=_('Phone'), required=False,
                            widget=forms.TextInput(
                                attrs={
                                    'class': 'form-control'
                                })
                            )

    type = SearchChoiceField(choices=Client.TYPES,
                             required=False, empty_label='---------',
                             label=_('Type'), field='type',
                             widget=forms.Select(
                                 attrs={
                                     'class': 'form-control',
                                 }))

    branch = SearchModelChoiceField(queryset=Branch.objects.all(),
                                    required=False,
                                    label=_('Branch'), field='branch',
                                    widget=forms.Select(
                                        attrs={
                                            'class': 'form-control',
                                        }))
