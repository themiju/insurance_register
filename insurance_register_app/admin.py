#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from . import models
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import Permission


class CustomUserAdmin(UserAdmin):
    model = models.User

    fieldsets = (
        (_('System information'), {
            'fields': ('username', 'password', 'start_url',
                       'date_joined', 'group', 'branch')
        }),
        (_('Personal information'), {
            # 'classes': ('collapse',),
            'fields': ('first_name', 'last_name',),
        }),
        (_('Advanced options'), {
            'classes': ('collapse',),
            # 'fields': ('is_active', 'is_staff', 'is_superuser', 'date_joined'),
            'fields': ('is_active', 'is_superuser', 'is_staff', 'groups', 'user_permissions', ),
        }),
    )

    search_fields = ['last_name', 'first_name', ]
    list_display = ('last_name', 'first_name', 'branch', 'last_login')
    list_display_links = list_display
    list_filter = ['groups']
    ordering = ['last_name', 'first_name', ]

    readonly_fields = ['date_joined', ]


admin.site.register(models.Paper)
admin.site.register(models.Client)
admin.site.register(models.Policy)
admin.site.register(models.InsuranceType)
admin.site.register(models.InsuranceCompany)
# admin.site.register(models.PolicySeries)
admin.site.register(models.Branch)
admin.site.register(models.User, CustomUserAdmin)
# admin.site.register(models.User)
admin.site.register(models.VehicleBrand)
admin.site.register(models.ExpenseType)

admin.site.register(Permission)
