#!/usr/bin/env python
# -*- coding: utf-8 -*-

# from django.test import TestCase
from unittest import TestCase
from .models import Policy, Branch, User, Client, VehicleBrand, \
    PoliciesImportAct, \
    InsuranceCompany, InsuranceType, \
    InsuredVehicle
from random import randint


class PolicyTests(TestCase):
    def test_string_representation(self):
        series = 'Testing series'
        number = str(randint(1, 10 ** 10))

        br = Branch(name='Testing branch')
        pol = Policy(series=series, number=number)

        self.assertEqual(str(pol), series + ' - ' + number)


# class InsuredTests(TestCase):
#     def setUp(self):
#         self.brand = VehicleBrand.objects.create(title='Test brand')
#         self.branch = Branch.objects.create()
#         self.company = InsuranceCompany.objects.create()
#         self.user = User.objects.create()
#         self.act = PoliciesImportAct.objects.create(branch=self.branch,
#                                                     company=self.company,
#                                                     recipient=self.user)
#         self.vehicle = InsuredVehicle.objects.create(brand=self.brand,
#                                                      model='Test model',
#                                                      tag='р386ва78')
#         self.type = InsuranceType.objects.create()
#         self.policy = Policy.objects.create(act=self.act, company=self.company,
#                                             type=self.type,
#                                             insured=self.vehicle) # type: Policy
#
#     # def test_multi(self):
#     #     client = Client()  # type: Client
#     #     client.first_name = 'John'
#     #     client.last_name = 'Doe'
#     #     client.comment = 'Test user'
#     #     client.save()
#     #
#     #     brand = VehicleBrand()   # type: VehicleBrand
#     #     brand.title = 'Test brand'
#     #     brand.save()
#     #
#     #     vehicle = InsurableVehicle()  # type: InsurableVehicle
#     #     vehicle.brand = brand
#     #     vehicle.model = 'Test model'
#     #     vehicle.tag = 'р386ва78'
#     #     vehicle.client = client
#     #     vehicle.save()
#     #
#     #     estate = InsurableRealEstate()  # type: InsurableRealEstate
#     #     estate.address = 'Test real estate address'
#     #     estate.client = client
#     #     estate.save()
#     #
#     #     insured_client = InsurableClient()  # type: InsurableClient
#     #     insured_client.client = client
#     #     insured_client.save()
#     #
#     #     self.assertTrue(insured_client in client.insurables)
#     #     self.assertTrue(vehicle in client.insurables)
#     #     self.assertTrue(estate in client.insurables)
#     def test_insurable_setter(self):
#         self.assertEqual(self.policy._insured_id, self.vehicle.pk)
#         self.assertEqual(self.policy._insured_type, 'InsuredVehicle')
#
#     def test_insurable_getter(self):
#         self.assertEqual(self.policy.insured, self.vehicle)
#
class InsuredTests(TestCase):
    def setUp(self):
        for brand_name in (
                'Audi', 'BMW', 'Ford', 'Honda', 'Hyundai', 'Kia', 'Lada (ВАЗ)',
                'Mazda',
                'Mercedes-Benz', 'Mitsubishi', 'Nissan', 'Renault', 'Skoda',
                'Toyota',
                'Volkswagen',):
            brand = VehicleBrand(title=brand_name)
            brand.save()

        client = Client(
            last_name='Тестовый',
            first_name='Тест',
            patronymic_name='Тестович'
        )
        client.save()

        type = InsuranceType.objects.first() or InsuranceType(title='ОСАГО')
        type.save()

        company = InsuranceCompany.objects.first() \
                  or InsuranceCompany(title='Росёбтагострах')
        company.save()

    # def test_insurable_setter(self):
    #     i = Insured.objects.get(pk=1)  # type: Insured
    #     i.tag = 'р386ва76'
    #     i.model = '21074'
    #     i.brand = 'Lada'
    #     i.save()
    #     self.assertEqual(i.tag, 'р386ва76')
    #     self.assertEqual(i.model, '21074')
    #     self.assertEqual(i.brand, 'Lada')
    #
    #     i.tag = 'а386аа78'
    #     self.assertEqual(i.tag, 'а386аа78')
    #     self.assertEqual(i.model, '21074')
    #     self.assertEqual(i.brand, 'Lada')
    #     i.save()
    #
    #     del i.tag
    #     self.assertFalse(hasattr(i, 'tag'))
    #     self.assertEqual(i.model, '21074')
    #     # self.assertEqual(i.brand, 'Lada')

    def test_multiply(self):
        client = Client.objects.first()
        company = InsuranceCompany.objects.first()
        brands = VehicleBrand.objects.all()
        insurance_type = InsuranceType.objects.first()
        for i in range(0, 100):
            brand = brands[randint(0, brands.count()-1)]
            insured = InsuredVehicle(
                brand=brand,
                model='Fake model',
                tag='р386ва76'
            )
            insured.save()
            policy = Policy(
                insured=insured,
                number=i,
                series='EEE',
                # client=client,
                company=company,
                type=insurance_type
            )
            policy.save()

        print('Done')

        # i = Insured.objects.get(pk=1)  # type: Insured
        # i.tag = 'р386ва76'
        # i.model = '21074'
        # i.brand = 'Lada'
        # i.save()
        # self.assertEqual(i.tag, 'р386ва76')
        # self.assertEqual(i.model, '21074')
        # self.assertEqual(i.brand, 'Lada')
        #
        # i.tag = 'а386аа78'
        # self.assertEqual(i.tag, 'а386аа78')
        # self.assertEqual(i.model, '21074')
        # self.assertEqual(i.brand, 'Lada')
        # i.save()
        #
        # del i.tag
        # self.assertFalse(hasattr(i, 'tag'))
        # self.assertEqual(i.model, '21074')
        # self.assertEqual(i.brand, 'Lada')
