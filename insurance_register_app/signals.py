#!/usr/bin/env python
# -*- coding: utf-8 -*-



from django.utils.translation import ugettext_lazy as _
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save, pre_delete
from django.core.exceptions import ObjectDoesNotExist

from insurance_register_app.models import AuditLog, User, AuditedObjectMixin
from .middleware import get_request


@receiver(pre_save)
def log_model_changes_pre(sender, instance, raw, using, update_fields, **kwargs):
    # Put current version of instance to its field, we'll use it in post_save
    if isinstance(instance, AuditedObjectMixin):
        try:
            instance.old_instance = sender.objects.get(pk=instance.id)
        except ObjectDoesNotExist:
            instance.old_instance = None


@receiver(post_save)
def log_model_changes_post(sender, instance, raw, using, update_fields, **kwargs):
    if not isinstance(instance, AuditedObjectMixin):
        return

    logs = []

    request = get_request()
    user = getattr(request, 'user', None)
    if not user:
        user = User.objects.filter(is_superuser=True).order_by('id').first()

    last_log = instance.logs.order_by('-change_number').first()
    change_number = 0
    if last_log:  # type: AuditLog
        change_number = last_log.change_number + 1
    for field in instance.logging_fields():
        field_new = instance.field_value_human(field)
        field_old = instance.old_instance.field_value_human(field) \
            if instance.old_instance else None

        if field_old != field_new:
            logs.append(AuditLog(field=field, value_old=field_old,
                                 value_new=field_new,
                                 object=instance, editor=user,
                                 change_number=change_number))

    AuditLog.objects.bulk_create(logs)


@receiver(pre_delete)
def log_model_changes_delete(sender, instance, using, **kwargs):
    if not isinstance(instance, AuditedObjectMixin):
        return

    request = get_request()
    user = getattr(request, 'user', None)
    if not user:
        user = User.objects.filter(is_superuser=True).order_by('id').first()

    log_entry = AuditLog(field='object', value_old=None,
                         value_new=_('Deleted'),
                         object=instance, editor=user)
    log_entry.save()

