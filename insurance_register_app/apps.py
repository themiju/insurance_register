#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class InsuranceRegisterAppConfig(AppConfig):
    name = 'insurance_register_app'
    verbose_name = _('Register of insurance policies')

    def ready(self):
        from . import signals
