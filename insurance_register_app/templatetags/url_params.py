#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django import template

from insurance_register_app.helpers import array_get

register = template.Library()


@register.filter(name='lookup')
def lookup(value, arg, default=None):
    try:
        res = array_get(value, arg, default=default)
    except TypeError:
        res = default
    except AttributeError:
        res = default

    return res
