#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.contrib.auth.base_user import BaseUserManager
from django.contrib.contenttypes.fields import GenericForeignKey, \
    GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.core.validators import RegexValidator, MaxValueValidator, \
    MinValueValidator
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.db.models.aggregates import Count, Sum
from django.db.models.query_utils import Q
from django.urls import reverse
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.core.exceptions import PermissionDenied
from collections import OrderedDict

from insurance_register_app.helpers import array_get
from functools import reduce

PAYMENT_TYPE_ALL = 'ALL'
PAYMENT_TYPE_BANK = 'BANK'
PAYMENT_TYPE_CASH = 'CASH'
PAYMENT_TYPES = (
    (PAYMENT_TYPE_BANK, _('Bank')),
    (PAYMENT_TYPE_CASH, _('Cash')),
)

PROLONGED_TYPE_P = 'PROLONGED'
PROLONGED_TYPE_N = 'NOT_PROLONGED'
PROLONGED_TYPES = (
    (PROLONGED_TYPE_P, _('Prolonged')),
    (PROLONGED_TYPE_N, _('Not prolonged')),
)

POLICY_TYPE_OSAGO = 'OSAGO'
POLICY_TYPE_KASKO = 'KASKO'
POLICY_TYPE_KASKO_V3 = 'KASKO_V3'
POLICY_TYPE_KASKO_V5 = 'KASKO_V5'
POLICY_TYPE_KASKO_V10 = 'KASKO_V10'
POLICY_TYPE_DOP397 = 'DOP397'
POLICY_TYPE_DOP398 = 'DOP398'
POLICY_TYPE_DOP331 = 'DOP331'
# доп 354 "АльфаРемонт"
POLICY_TYPE_DOP354 = 'DOP354'
POLICY_TYPE_NS_SPORT = 'NSSPORT'
POLICY_TYPE_VZROSLY = 'VZROS'
POLICY_TYPE_MIGRANT = 'MIGRANT'
POLICY_TYPE_KIS = 'KIS'
POLICY_TYPE_IMUSHESTVO = 'IMUSH'
POLICY_TYPE_DIADCARD = 'DGCARD'
POLICY_TYPE_DMS_MITE = 'DMSMITE'
POLICY_TYPE_NS = 'NS'
POLICY_TYPE_AUTOSAFE = 'AUSAFE'
POLICY_TYPE_EPOLICY = 'EPOLICY'
POLICY_TYPE_TICKET = 'TICKET'
POLICY_TYPE_CARINC = 'CARINC'
#081 Добрые соседи.
POLICY_TYPE_DBRSSD081 = 'DBRSSD081'
# тип страхования "Зеленая карта"
POLICY_TYPE_ZELCART = 'ZELCART'

# 1. ПС от НС "Дорожный"
POLICY_TYPE_PSNSDRJ = 'PSNSDRJ'
# 2. ПС от НС "Мое здоровье"
POLICY_TYPE_PSNSMYZD = 'PSNSMYZD'
# 3. ПС от НС "Защита +"
POLICY_TYPE_PSNSZSHPL = 'PSNSZSHPL'

POLICY_TYPES = (
    (POLICY_TYPE_OSAGO, _('OSAGO')),
    (POLICY_TYPE_DIADCARD, _('DIAG. CARD')),
    (POLICY_TYPE_DOP397, _('ADD. 397')),
    (POLICY_TYPE_DOP331, _('ADD. 331')),
    (POLICY_TYPE_DOP354, _('ADD. 354')),
    (POLICY_TYPE_KIS, _('KIS')),
    (POLICY_TYPE_IMUSHESTVO, _('R. ESTATE')),
    (POLICY_TYPE_KASKO, _('KASKO')),
    (POLICY_TYPE_KASKO_V3, _('KASKO V 3')),
    (POLICY_TYPE_KASKO_V5, _('KASKO V 5')),
    (POLICY_TYPE_KASKO_V10, _('KASKO V 10')),
    (POLICY_TYPE_VZROSLY, _('ADL')),
    (POLICY_TYPE_MIGRANT, _('MIGRANT')),
    (POLICY_TYPE_NS_SPORT, _('TA-SPORT')),
    (POLICY_TYPE_DOP398, _('ADD. 398')),
    (POLICY_TYPE_DMS_MITE, _('DMS MITE')),
    (POLICY_TYPE_NS, _('NS')),
    (POLICY_TYPE_AUTOSAFE, _('AUTOSAFE')),
    (POLICY_TYPE_EPOLICY, _('E-POLICY')),
    (POLICY_TYPE_TICKET, _('TICKET')),
    (POLICY_TYPE_CARINC, _('CAR INCIDENT AID')),
    (POLICY_TYPE_DBRSSD081, _('GOOD NEIGHBOURHOOD 081')),
    (POLICY_TYPE_PSNSDRJ, _('ACCIDENT ROAD')),
    (POLICY_TYPE_PSNSMYZD, _('ACCIDENT HEALTH')),
    (POLICY_TYPE_PSNSZSHPL, _('ACCIDENT PROTECTION PLUS')),
    (POLICY_TYPE_ZELCART, _('GREEN CARD')),
)


def get_object_by_class_and_pk(cls_pk, obj_pk):
    cls = ContentType.objects.get(pk=cls_pk)
    obj = cls.get_object_for_this_type(id=obj_pk)

    return obj


class AuditLog(models.Model):
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'),
                                      db_index=True)
    
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'),
                                     db_index=True)

    object_type_id = models.PositiveIntegerField(verbose_name=_("Object type ID"),
                                                 db_index=True,)
    object_id = models.PositiveIntegerField(verbose_name=_("Object ID"),
                                            db_index=True)

    edited_by = models.PositiveIntegerField(verbose_name=_('Editor'),
                                            db_index=True)

    value_old = models.CharField(max_length=4000, verbose_name=_('Old value'),
                                 blank=True, null=True)
    value_new = models.CharField(max_length=4000, verbose_name=_('New value'),
                                 blank=True, null=True)
    field = models.CharField(max_length=100, verbose_name=_('Field name'))
    change_number = models.IntegerField(default=0, db_index=True,
                                        verbose_name=_('Change number'))

    @property
    def object(self):
        # manager = ContentTypeManager()
        # cls = manager.get_for_id(self.object_type_id)
        cls = ContentType.objects.get(pk=self.object_type_id)
        obj = cls.get_object_for_this_type(id=self.object_id)

        return obj

    @object.setter
    def object(self, obj):
        object_type = ContentType.objects.get_for_model(obj)
        self.object_type_id = object_type.id
        self.object_id = obj.id

    @property
    def editor(self):
        editor = User.objects.get(pk=self.edited_by)
        return editor or None

    @editor.setter
    def editor(self, user):
        self.edited_by = user.id

    @classmethod
    def logs_for_object(cls, obj):
        object_type = ContentType.objects.get_for_model(obj)
        logs = cls.objects \
            .filter(object_type_id=object_type.id) \
            .filter(object_id=obj.id)

        return logs

    class Meta:
        verbose_name = _('audit log entry')
        verbose_name_plural = _('audit log entries')
        index_together = [
            ['object_type_id', 'object_id'],
        ]

class AuditedObjectMixin(models.Model):
    @classmethod
    def logging_fields(cls):
        return [f.name for f in cls._meta.get_fields()]

    @property
    def logs(self):
        return AuditLog.logs_for_object(self)

    class Meta:
        abstract = True

    def field_value_human(self, field):
        """Returns human readable value of specified field"""
        value = getattr(self, field, None)
        if type(value) in (str, str):
            try:
                method = getattr(self, 'get_' + field + '_display')
                value = method()
            except AttributeError:
                pass
        else:
            value = str(value)

        return value


class Branch(AuditedObjectMixin, models.Model):
    """
    Local branch of KaskOsago
    """
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))
    name = models.CharField(max_length=300, verbose_name=_('Name'))
    sequence = models.PositiveSmallIntegerField(default=1,
                                                verbose_name=_('Sequence'),
                                                validators=[
                                                    MaxValueValidator(100),
                                                    MinValueValidator(1)])
    comment = models.TextField(max_length=21500, blank=True, null=True,
                               verbose_name=_('Comment'))
    visible = models.BooleanField(default=True, verbose_name=_('Is visible'))

    class Meta:
        verbose_name = _('branch')
        verbose_name_plural = _('branches')
        ordering = ["name"]

    def __str__(self):
        return self.__unicode__()

    def __unicode__(self):
        return self.name


# class PolicySeries(models.Model):
#     """
#     Policy series. Usually it's upper case string.
#     """
#     created_at = models.DateTimeField(auto_now_add=True,
#                                       verbose_name=_('Created at'))
#     edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))
#     title = models.CharField(max_length=10, verbose_name=_('Title'),
#                              unique=True)
#     comment = models.TextField(max_length=21500, blank=True, null=True,
#                                verbose_name=_('Comment'))
#
#     class Meta:
#         verbose_name = _('Policy series')
#         verbose_name_plural = _('Policy series')
#
#     def __unicode__(self):
#         return self.title


class InsuranceType(models.Model):
    """
    Policy insurance type.
    """
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))
    title = models.CharField(max_length=10, verbose_name=_('Title'),
                             unique=True)
    comment = models.TextField(max_length=21500, blank=True, null=True,
                               verbose_name=_('Comment'))

    class Meta:
        verbose_name = _('insurance type')
        verbose_name_plural = _('insurance types')

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.__unicode__()


class InsuranceCompany(models.Model):
    """
    Insurance company.
    """
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))
    title = models.CharField(max_length=254, verbose_name=_('Title'),
                             unique=True)
    comment = models.TextField(max_length=21500, blank=True, null=True,
                               verbose_name=_('Comment'))

    class Meta:
        verbose_name = _('insurance company')
        verbose_name_plural = _('insurance companies')

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.__unicode__()


# Copied from django.contrib.auth.models
class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, username, password, **extra_fields):
        """
        Creates and saves a User with the given username and password.
        """
        if not username:
            raise ValueError('The given username must be set')
        username = self.model.normalize_username(username)
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, username, password=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(username, password, **extra_fields)

    def create_superuser(self, username, password, **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(username, password, **extra_fields)


class User(AbstractBaseUser, AuditedObjectMixin, PermissionsMixin):
    def get_short_name(self):
        name = []
        if self.first_name:
            name.append(self.first_name[0])

        return ' '.join(name)

    def get_full_name(self):
        return ' '.join([self.last_name, self.first_name])

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    OPERATOR = 'OP'
    ADMINISTRATOR = 'AD'
    CHIEF = 'CH'
    GROUPS = (
        (OPERATOR, _('Operator')),
        (ADMINISTRATOR, _('Administrator')),
        (CHIEF, _('Chief')),
    )

    username = models.CharField(max_length=200, unique=True, blank=False,
                                null=False, verbose_name=_('User name'))
    first_name = models.CharField(max_length=50, verbose_name=_('Name'))
    last_name = models.CharField(max_length=250, verbose_name=_('Last name'))
    is_active = models.BooleanField(default=True, verbose_name=_('Is active'))
    is_staff = models.BooleanField(default=False, verbose_name=_('Is staff'))
    is_superuser = models.BooleanField(default=False,
                                       verbose_name=_('Is superuser'))
    date_joined = models.DateTimeField(auto_now_add=True,
                                       verbose_name=_('Date of join'))
    last_login = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Date of last login'))
    start_url = models.CharField(max_length=2047,
                                 verbose_name=_('Start page'),
                                 default='policies:list')
    group = models.CharField(choices=GROUPS, max_length=2,
                             verbose_name=_('User group'),
                             default=OPERATOR)
    branch = models.ForeignKey(Branch, default=1, verbose_name=_('Branch'))
    importants_last_reading_time = models.DateTimeField(
        blank=True, null=True,
        verbose_name=_('Last date and time of important policies reading'))

    objects = UserManager()

    @property
    def filtering_branch(self):
        branch = self.branch
        if self.has_perm('insurance_register_app.access_any_branch'):
            branch = None
        if self.has_perm('insurance_register_app.access_its_branch_only'):
            branch = self.branch

        return branch

    def check_user_access(self, object_branch):
        granted = True
        user_branch = self.filtering_branch
        if user_branch:
            granted = object_branch == user_branch

        return granted

    @property
    def name(self):
        name = ' '.join([self.first_name, self.last_name, ]).strip()
        name = name + ' (' + self.username + ')' if name else self.username
        return name

    @property
    def is_operator(self):
        return self.OPERATOR == self.group

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')
        permissions = (
            ('access_its_branch_only', _('Access its branch only')),
            ('access_any_branch', _('Access any branch')),
        )

    def __unicode__(self):
        return self.get_full_name()

    def __str__(self):
        return self.__unicode__()


class PoliciesImportAct(AuditedObjectMixin, models.Model):
    """
    Act of importing bunch of policies
    """
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))

    first_policy_number = models.CharField(max_length=10, verbose_name=_(
        'First policy number'))
    last_policy_number = models.CharField(max_length=10,
                                          verbose_name=_('Last policy number'))

    comment = models.TextField(max_length=21500, blank=True, null=True,
                               verbose_name=_('Comment'), default='')
    series = models.CharField(max_length=10, verbose_name=_('Series'))
    branch = models.ForeignKey(Branch, verbose_name=_('Branch'),
                               # default=Branch.objects.first()
                               )
    recipient = models.ForeignKey(User, verbose_name=_('Recipient'))
    company = models.ForeignKey(InsuranceCompany,
                                verbose_name=_('Insurance company'),
                                # default=InsuranceCompany.objects.first()
                                )
    type = models.CharField(max_length=10, choices=POLICY_TYPES,
                            verbose_name=_('Insurance type'))

    STATUS_NEW = 'NEW'
    STATUS_IN_BRANCH = 'IN_BRANCH'
    STATUS_CONF_DELV = 'CONF_DELV'
    STATUSES = (
        (STATUS_NEW, _('New (send to branch)')),
        (STATUS_IN_BRANCH, _('In branch')),
        (STATUS_CONF_DELV, _('Confirmed delivery')),
    )
    status = models.CharField(choices=STATUSES, max_length=20,
                              default=STATUS_NEW,
                              verbose_name=_('Status'))

    PRINT_FIELDS = [
        'first_policy_number', 'last_policy_number',
        'comment', 'series', 'branch', 'company',
    ]

    def get_print_fields(self):
        res = []
        for field in self._meta.get_fields():
            if field.name in self.PRINT_FIELDS:
                res.append({
                    'value': getattr(self, field.name),
                    'name': field.verbose_name
                })
        return res

    def make_policies(self):
        cnt = 0
        strlen = len(self.first_policy_number)
        first = int(self.first_policy_number.lstrip('0'))
        last = int(self.last_policy_number.lstrip('0'))

        while (first + cnt) <= last:
            policy = Policy()  # type: Policy
            policy.series = self.series
            policy.number = str(first + cnt).zfill(strlen)
            policy.branch = self.branch
            policy.import_act = self
            policy.company = self.company
            policy.type = self.type
            policy.save()
            cnt += 1

        return cnt

    @property
    def is_new(self):
        return self.status == self.STATUS_NEW

    class Meta:
        verbose_name = _('policy import act')
        verbose_name_plural = _('policy import acts')
        permissions = (
            ('view_policies_import_acts', _('View policies import acts')),
            ('import_policies_act_print', _('Print policies import act')),
            ('import_policies_act_in_branch',
             _('Approve import act delivered in branch')),
        )

    def __unicode__(self):
        return "%s - %s - %d" % (self.series, self.branch, self.id)

    def __str__(self):
        return self.__unicode__()


class Client(AuditedObjectMixin, models.Model):
    TYPE_PERSON = 'P'
    TYPE_ORGANIZATION = 'O'
    TYPES = (
        (TYPE_ORGANIZATION, _('Legal entity')),
        (TYPE_PERSON, _('Individual')),
    )

    MAX_SIMILAR_CLIENTS_COUNT = 10

    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))

    # Common fields
    comment = models.TextField(max_length=21500, blank=True, null=True,
                               verbose_name=_('Comment'), default='')
    branch = models.ForeignKey(Branch, verbose_name=_('Branch'))
    type = models.CharField(max_length=1, verbose_name=_('Type'),
                            db_index=True,
                            choices=TYPES, default=TYPE_PERSON)

    # Person-specific fields
    last_name = models.CharField(max_length=100, verbose_name=_('Last name'),
                                 blank=True, null=True, db_index=True)
    first_name = models.CharField(max_length=100, verbose_name=_('First name'),
                                  blank=True, null=True, db_index=True)
    patronymic_name = models.CharField(max_length=100, blank=True, null=True,
                                       verbose_name=_('Patronymic name'),
                                       db_index=True)
    date_of_birth = models.DateField(blank=True, null=True,
                                     verbose_name=_('Date of birth'))
    phone = models.CharField(blank=True, null=True, max_length=17,
                             verbose_name=_('Phone'))

    # Organization-specific fields
    title = models.CharField(max_length=255, verbose_name=_('Title'),
                             blank=True, null=True, db_index=True)
    # ИНН
    tax_number = models.CharField(max_length=12, verbose_name=_('Tax number'),
                                  blank=True, null=True, db_index=True)
    address_legal = models.CharField(max_length=190, db_index=True,
                                     verbose_name=_('Address legal'),
                                     blank=True, null=True)
    address_actual = models.CharField(max_length=190, db_index=True,
                                      verbose_name=_('Address actual'),
                                      blank=True, null=True, )

    class Meta:
        verbose_name = _('client')
        verbose_name_plural = _('clients')

    def __unicode__(self):
        return self.full_name

    def __str__(self):
        return self.__unicode__()

    @property
    def full_name(self):
        name = ''
        if self.TYPE_PERSON == self.type:
            name = ' '.join([
                self.last_name or '',
                self.first_name or '',
                self.patronymic_name or '',
            ])
        if self.TYPE_ORGANIZATION == self.type:
            name = self.title or ''

        name = name.strip() or _('<Client has not name or title>')

        return str(name)

    @property
    def available_insured_types(self):
        return Insured.get_all_classes()

    @property
    def insured(self):
        insureds = []
        for cls in Insured.get_all_classes():
            insureds += cls.objects.filter(client=self).all()

        return insureds

    @property
    def policies(self):
        policies = []
        for item in self.insured:
            policies += item.policies.all()

        return policies

    PRINT_FIELDS = [
        'last_name', 'first_name', 'patronymic_name', 'date_of_birth',
        'comment', ]

    @classmethod
    def get_similar(cls, data):
        clients = Client.objects
        unique_fields = ['last_name', 'first_name', 'patronymic_name', 'title',
                         'tax_number', ]
        for field in unique_fields:
            if field in data and data[field]:
                dict = {field + '__icontains': data[field]}
                q_object = Q(**dict)
                clients = clients.filter(q_object)

        return clients.all()

    def get_print_fields(self):
        res = []
        for field in self._meta.get_fields():
            if field.name in self.PRINT_FIELDS:
                res.append({
                    'value': getattr(self, field.name),
                    'name': field.verbose_name
                })
        return res


# class Insured(models.Model):
#     TYPE_VEHICLE = 'vehi'
#     TYPE_REALESTATE = 'rees'
#     TYPE_PERSON = 'pers'
#     TYPES = (
#         (TYPE_VEHICLE, _('Vehicle')),
#         (TYPE_REALESTATE, _('Real estate')),
#         (TYPE_PERSON, _('Person')),
#     )
#
#     client = models.ForeignKey(Client, verbose_name=_('Insured owner'),
#                                related_name='insured')
#     type = models.CharField(choices=TYPES, max_length=4,
#                             verbose_name=_('Type of insured object'))
#
#     def get_class_name(self):
#         return self.__class__.__name__
#
#     @classmethod
#     def get_class_data(cls):
#         return {
#             'name': cls.__name__,
#             'verbose_name': cls._meta.verbose_name,
#         }
#
#     @classmethod
#     def get_all_classes(cls):
#         return cls.__subclasses__()
#
#     @classmethod
#     def get_class(cls, class_name):
#         return globals()[class_name]
#
#     @classmethod
#     def get_object(cls, class_name, pk):
#         insured_class = cls.get_class(class_name=class_name)
#         insured = insured_class.objects.get(pk=pk)
#
#         return insured
#
#     def detail(self, name):
#         value = ''
#         try:
#             value = getattr(self, name)
#         except AttributeError:
#             pass
#
#         return value
#
#     @property
#     def verbose_name(self):
#         name = self.get_type_display()
#         type_verbose_names = {
#             self.TYPE_VEHICLE: "%s %s (%s)" % (self.detail('brand'),
#                                                self.detail('model'),
#                                                self.detail('tag')),
#             self.TYPE_REALESTATE: "%s" % (self.detail('address')),
#             self.TYPE_PERSON: "%s" % (self.detail('person')),
#         }
#         if self.type in type_verbose_names:
#             name = name + ' ' +type_verbose_names[self.type]
#
#         return name
#
#     def __unicode__(self):
#         return self.verbose_name
#
#     @property
#     def details_dict(self):
#         dict = {}
#         for field in self.details.all():
#             dict[field.name] = field.value
#
#         return dict
#
#     def __init__(self, *args, **kwargs):
#         super(Insured, self).__init__(*args, **kwargs)
#         for detail in self.details.all():
#             setattr(self, detail.field.name, detail.value)
#
#     # @property
#     # def details(self):
#     #     if not self._details:
#     #         for field in self.details.all():
#     #             self._details[field] = field
#     #
#     #     return self._details
#     #
#     # @details.setter
#     # def details(self, name, value):
#     #     self._details[''] = insured.pk
#     #         self._insured_type = insured.get_class_name()
#     #     else:
#     #         raise ValueError(_('Insured object has to be Insured instance'))
#     #
#     # @details.deleter
#     # def details(self):
#     #     del self._insured_id
#     #     del self._insured_type
#
#     # def __getattr__(self, name):
#     #     # return getattr(self._obj, name)
#     #     if not self._details:
#     #         details_manager = getattr(self._obj, 'details')
#     #         for field in details_manager.all():
#     #             self._details[field.name] = field.value
#     #
#     #     if name in self._details:
#     #         val = self._details[name]
#     #     else:
#     #         val = getattr(self._obj, name)
#     #
#     #     return val
#
#     # def __setattr__(self, item, value):
#     #     virtual_field = False
#     #     for field in self.details.all():
#     #         virtual_field = virtual_field or (item == field.name)
#     #
#     #     if virtual_field:
#     #         self._details[item] = value
#     #     else:
#     #         super(Insured, self).__setattr__(item, value)
#     #
#     #
#     #
#     #
#     #
#     # class Meta:
#     #     abstract = True
#
#     def save(self, force_insert=False, force_update=False, using=None,
#              update_fields=None):
#         # First, put all fields to dictionary.
#         details = {}
#         # If field of system dictionary is in our insured object, we put it to
#         # local dict and remove it from object to avoid saving this field to DB
#         for field in InsuredField.objects.all():
#             try:
#                 details[field.name] = getattr(self, field.name)
#                 delattr(self, field.name)
#             except AttributeError:
#                 pass
#
#         # Performing parent save method
#         super(Insured, self).save(force_insert, force_update, using,
#                                   update_fields)
#
#         # Clear all current fields
#         self.details.all().delete()
#
#         # Save new fields and its values
#         for field_name in details:
#             insured_field = InsuredField.objects.get(name=field_name)
#             value = details[field_name]
#             df = InsuredDetails(insured=self, field=insured_field,
#                                 value=value)
#             df.save()
#             # Set field value to insured object (cause it was deleted before)
#             setattr(self, field_name, value)
#
#
# class InsuredField(models.Model):
#     name = models.CharField(max_length=20)
#
#     def __unicode__(self):
#         return self.name
#
#
# class InsuredDetails(models.Model):
#     insured = models.ForeignKey(Insured, related_name='details',
#                                 on_delete=models.CASCADE)
#
#     field = models.ForeignKey(InsuredField, related_name='values',
#                               on_delete=models.CASCADE)
#
#     value = models.CharField(max_length=4000)
#
#     @property
#     def name(self):
#         return self.field.name
#
#     def __unicode__(self):
#         return self.name + ' - ' + self.value
class UtilizationAct(models.Model):
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))
    parent_act = models.ForeignKey('self', blank=True, null=True,
                                   verbose_name=_('Parent act'),
                                   related_name='attached_acts')

    class Meta:
        verbose_name = _('utilization act')
        verbose_name_plural = _('utilization acts')

    def __unicode__(self):
        return _("#%d") % self.pk

    def __str__(self):
        return self.__unicode__()

    @property
    def policies_all(self):
        return reduce(lambda id_list, attached_act:
                      id_list + list(attached_act.policies_all),
                      self.attached_acts.all(),
                      list(self.policies.all()))

    @property
    def policies_count(self):
        return len(self.policies_all)

    @property
    def policies_cost(self):
        return sum(policy.cost for policy in self.policies_all)


class Policy(AuditedObjectMixin, models.Model):
    STATUS_NEW = 'NEW'
    STATUS_ISSUED = 'ISSD'
    STATUS_CORRUPTED = 'CORR'
    STATUS_CORRUPTED_CANDIDAT = 'CORC'
    STATUS_ARCHIVED = 'ARCH'
    STATUS_CANCELED = 'CANC'

    STATUSES = (
        (STATUS_NEW, _('New')),
        (STATUS_ISSUED, _('Issued')),
        (STATUS_CORRUPTED, _('Corrupted')),
        (STATUS_CORRUPTED_CANDIDAT, _('Corrupted candidat')),
        (STATUS_ARCHIVED, _('Archived')),
        (STATUS_CANCELED, _('Canceled')),
    )
    STATUSES_DICT = dict(STATUSES)

    TYPE_OSAGO = POLICY_TYPE_OSAGO
    TYPE_KASKO = POLICY_TYPE_KASKO
    TYPE_KASKO_V3 = POLICY_TYPE_KASKO_V3
    TYPE_KASKO_V5 = POLICY_TYPE_KASKO_V5
    TYPE_KASKO_V10 = POLICY_TYPE_KASKO_V10
    TYPE_DOP397 = POLICY_TYPE_DOP397
    TYPE_DOP398 = POLICY_TYPE_DOP398
    TYPE_DOP331 = POLICY_TYPE_DOP331
    TYPE_DOP354 = POLICY_TYPE_DOP354
    TYPE_NS_SPORT = POLICY_TYPE_NS_SPORT
    TYPE_VZROSLY = POLICY_TYPE_VZROSLY
    TYPE_MIGRANT = POLICY_TYPE_MIGRANT
    TYPE_KIS = POLICY_TYPE_KIS
    TYPE_IMUSHESTVO = POLICY_TYPE_IMUSHESTVO
    TYPE_DIADCARD = POLICY_TYPE_DIADCARD
    TYPE_DMS_MITE = POLICY_TYPE_DMS_MITE
    TYPE_NS = POLICY_TYPE_NS
    TYPE_AUTOSAFE = POLICY_TYPE_AUTOSAFE
    TYPE_EPOLICY = POLICY_TYPE_EPOLICY
    TYPE_TICKET = POLICY_TYPE_TICKET
    TYPE_CARINC = POLICY_TYPE_CARINC
    TYPE_DBRSSD081 = POLICY_TYPE_DBRSSD081
    TYPE_PSNSDRJ = POLICY_TYPE_PSNSDRJ
    TYPE_PSNSMYZD = POLICY_TYPE_PSNSMYZD
    TYPE_PSNSZSHPL = POLICY_TYPE_PSNSZSHPL
    TYPE_ZELCART = POLICY_TYPE_ZELCART

    TYPES = POLICY_TYPES

    TYPES_DICT = dict(TYPES)

    created_at = models.DateTimeField(auto_now_add=True, db_index=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'),
                                     db_index=True)
    series = models.CharField(max_length=10, verbose_name=_('Policy series'),
                              blank=True, null=True, db_index=True)
    number = models.CharField(max_length=30, verbose_name=_('Number'),
                              db_index=True)
    date_issued = models.DateTimeField(blank=True, null=True, db_index=True,
                                       verbose_name=_('Date issued'))
    comment = models.TextField(max_length=21500, blank=True, null=True,
                               verbose_name=_('Comment'), default='')
    branch = models.ForeignKey(Branch, verbose_name=_('Branch'))
    import_act = models.ForeignKey(PoliciesImportAct,
                                   verbose_name=_('Import act'),
                                   null=True, blank=True,
                                   on_delete=models.CASCADE)
    utilization_act = models.ForeignKey(UtilizationAct,
                                        verbose_name=_('Utilization act'),
                                        null=True, blank=True,
                                        on_delete=models.SET_NULL,
                                        related_name='policies')
    status = models.CharField(max_length=4, choices=STATUSES, db_index=True,
                              verbose_name=_('Status of policy'),
                              default=STATUS_NEW)
    company = models.ForeignKey(InsuranceCompany,
                                verbose_name=_('Insurance company'))
    type = models.CharField(max_length=10, choices=TYPES, db_index=True,
                            verbose_name=_('Insurance type'))
    cost = models.DecimalField(max_digits=9, decimal_places=2, null=True,
                               blank=True, verbose_name=_('Cost'))
    payment_type = models.CharField(max_length=4, choices=PAYMENT_TYPES,
                                    db_index=True,
                                    verbose_name=_('Payment type'),
                                    default=PAYMENT_TYPE_CASH)
    receipt_number = models.CharField(max_length=20, db_index=True,
                                      verbose_name=_('Receipt number'),
                                      null=True, blank=True, default=None)

    insured_type = models.ForeignKey(ContentType, blank=True, null=True,
                                     verbose_name=_("Insured type"))
    insured_id = models.PositiveIntegerField(blank=True, null=True,
                                             editable=True, db_index=True,
                                             verbose_name=_("Insured ID"))
    insured = GenericForeignKey('insured_type', 'insured_id')
    important = models.BooleanField(default=False, verbose_name=_('Problem'),
                                    db_index=True)
    prolong_parent = models.ForeignKey('self', blank=True, null=True, related_name='prolong_child')

    class Meta:
        verbose_name = _('policy')
        verbose_name_plural = _('policies')

    @property
    def full_number(self):
        number = self.number
        if self.series:
            number = number + ' - ' + self.series

        return number

    @property
    def instant_save_fields(self):
        return 'comment', 'important', 'receipt_number',

    @property
    def is_readonly(self):
        # readonly = False or self.status in [Policy.STATUS_ISSUED,
        #                                     Policy.STATUS_CORRUPTED]
        readonly = self.utilization_act is not None

        return readonly

    def delete_method(self):
        if self.type in [self.TYPE_OSAGO, self.TYPE_TICKET]:
            # Policy is utilized and can's be deleted
            if self.is_readonly:
                raise PermissionDenied

            self.date_issued = None
            self.status = self.STATUS_NEW
            self.cost = None
            self.insured = None
            self.save()
        else:
            self.delete()

    def __unicode__(self):
        return self.full_number

    def __str__(self):
        return self.__unicode__()

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        original_number = getattr(self, '__original_number')
        if self.type in [self.TYPE_OSAGO, self.TYPE_TICKET] and original_number:
            setattr(self, 'number', getattr(self, '__original_number'))

        super(Policy, self).save(force_insert, force_update, using,
                                 update_fields)

    def __init__(self, *args, **kwargs):
        super(Policy, self).__init__(*args, **kwargs)
        setattr(self, '__original_number', getattr(self, 'number'))


class Invoice(AuditedObjectMixin, models.Model):
    created_at = models.DateTimeField(auto_now_add=True, db_index=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'),
                                     db_index=True)
    number = models.CharField(max_length=30, verbose_name=_('Number'),
                              db_index=True)
    date_issued = models.DateTimeField(blank=True, null=True, db_index=True,
                                       verbose_name=_('Date issued'))
    purpose = models.TextField(max_length=21500, blank=True, null=True,
                               verbose_name=_('Purpose'), default='')

    policy = models.OneToOneField(Policy, verbose_name=_('Policy'),
                                  on_delete=models.CASCADE, related_name='invoice')

    def is_non_empty(self):
        return len(self.number or "") > 1

    class Meta:
        verbose_name = _('invoice')
        verbose_name_plural = _('invoices')


class Insured(models.Model):
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    policies = GenericRelation(Policy, content_type_field='insured_type',
                               object_id_field='insured_id')

    class Meta:
        abstract = True

    @classmethod
    def class_name(cls):
        return cls.__name__ or ''

    @classmethod
    def policy_form(cls):
        return 'PolicyForm'

    @classmethod
    def policy_issue_view(cls):
        return 'policy_issue_default'

    @classmethod
    def policy_types(cls):
        types = []
        for type in cls.policy_types_list():
            types.append((type, Policy.TYPES_DICT[type]))

        return types

    @classmethod
    def verbose_name(self):
        return self._meta.verbose_name or ''

    @classmethod
    def get_or_create(cls, ins_type, ins_id):
        insured = None
        subcls = cls.get_class(ins_type)
        if subcls:
            if ins_id:
                insured = subcls.objects.get(pk=ins_id)
            else:
                insured = subcls()

        return insured

    @classmethod
    def get_class(cls, sub_class_string):
        sub_class = None
        subclasses = cls.get_all_classes()
        for subclass in subclasses:
            if subclass.class_name() == sub_class_string:
                sub_class = subclass

        return sub_class

    @classmethod
    def get_all_classes(cls):
        return cls.__subclasses__()

    def create_policy(self, search_data):
        return Policy(insured=self)


class VehicleBrand(models.Model):
    title = models.CharField(max_length=254, verbose_name=_('Name'))

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.__unicode__()

    class Meta:
        verbose_name = _('vehicle brand')
        verbose_name_plural = _('vehicles brands')

    def get_absolute_url(self):
        return reverse('vehicle_brands:edit', kwargs={
            'pk': self.id,
        })


vehicle_tag = RegexValidator(regex=r'(?u)^\w\d\d\d\w\w\d{2,3}$',
                             message=_(
                                 'Vehicle tag has aNNNaaNN or aNNNaaNNN mask')
                             )


class InsuredVehicle(Insured):
    brand = models.ForeignKey(VehicleBrand, verbose_name=_('Vehicle brand'))
    model = models.CharField(max_length=254, verbose_name=_('Model'))
    tag = models.CharField(max_length=9, null=True, blank=True,
                           verbose_name=_('Vehicle tag'))

    def __unicode__(self):
        return self.brand.title + ' ' + self.model + ' (' + self.tag + ')'

    def __str__(self):
        return self.__unicode__()

    @classmethod
    def form(cls):
        return 'InsuredVehicleForm'

    @classmethod
    def policy_form(cls):
        return 'PolicyVehicleForm'

    @classmethod
    def policy_types_list(cls):
        return [Policy.TYPE_OSAGO, Policy.TYPE_KASKO, Policy.TYPE_DIADCARD,
                Policy.TYPE_AUTOSAFE, Policy.TYPE_EPOLICY, Policy.TYPE_TICKET,
                Policy.TYPE_KASKO_V3, Policy.TYPE_KASKO_V5,
                Policy.TYPE_KASKO_V10, Policy.TYPE_ZELCART, ]

    @classmethod
    def policy_issue_view(cls):
        return 'policy_issue_vehicle'

    def create_policy(self, search_data):
        # Use already existed policy (imported ones)
        if ('cleaned_data' in search_data) and \
           ('series' in search_data.cleaned_data) and \
           ('number' in search_data.cleaned_data) and \
           ('act' in search_data.cleaned_data):
                policy = Policy.objects.\
                    filter(series=search_data.cleaned_data['series']).\
                    filter(number=search_data.cleaned_data['number']).\
                    filter(act=search_data.cleaned_data['act']).\
                    first()
                if policy:
                    del search_data.cleaned_data['series']
                    del search_data.cleaned_data['number']
                    del search_data.cleaned_data['act']

        return Policy(insured=self)

    class Meta:
        verbose_name = _('vehicle')
        verbose_name_plural = _('vehicles')


class InsuredRealEstate(Insured):
    address = models.CharField(max_length=4000, verbose_name=_('Address'))

    def __unicode__(self):
        return self.address

    def __str__(self):
        return self.__unicode__()

    @classmethod
    def form(cls):
        return 'InsuredRealestateForm'

    @classmethod
    def policy_types_list(cls):
        return [Policy.TYPE_IMUSHESTVO, Policy.TYPE_KIS, Policy.TYPE_DOP331, Policy.TYPE_DBRSSD081, Policy.TYPE_DOP354,]

    class Meta:
        verbose_name = _('real estate')
        verbose_name_plural = _('real estates')


class InsuredPerson(Insured):
    person = models.ForeignKey(Client, verbose_name=_('Insured person'),
                               related_name='insurance', blank=True, null=True)
    last_name = models.CharField(max_length=100, verbose_name=_('Last name'),
                                 blank=True, null=True)
    first_name = models.CharField(max_length=100, verbose_name=_('First name'),
                                  blank=True, null=True)
    patronymic_name = models.CharField(max_length=100, blank=True, null=True,
                                       verbose_name=_('Patronymic name'))

    def __unicode__(self):
        return ' '.join([self.last_name, self.first_name, self.patronymic_name, ]).strip()

    def __str__(self):
        return self.__unicode__()

    @classmethod
    def form(cls):
        return 'InsuredPersonForm'

    @classmethod
    def policy_types_list(cls):
        return [
            Policy.TYPE_MIGRANT,
            Policy.TYPE_NS_SPORT,
            Policy.TYPE_VZROSLY,
            Policy.TYPE_DOP397,
            Policy.TYPE_DOP398,
            Policy.TYPE_DMS_MITE,
            Policy.TYPE_NS,
            Policy.TYPE_CARINC,
            Policy.TYPE_PSNSDRJ,
            Policy.TYPE_PSNSMYZD,
            Policy.TYPE_PSNSZSHPL,
        ]

    class Meta:
        verbose_name = _('insured person')
        verbose_name_plural = _('insured persons')


# class PolicyVehicle(Policy):
#
#
# class PolicyRealEstate(Policy):
#     insured = models.ForeignKey(InsuredRealEstate, verbose_name=_('Insured real estate'))
#
#
# class PolicyPerson(Policy):
#     insured = models.ForeignKey(Insu, verbose_name=_('Insured real estate'))


class Paper(AuditedObjectMixin, models.Model):
    TYPE_PASSPORT = 'PASP'
    TYPE_DRIVER_LICENSE = 'DRLI'

    TYPES = (
        (TYPE_PASSPORT, _('Passport')),
        (TYPE_DRIVER_LICENSE, _('Driver license')),
    )

    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))

    type = models.CharField(max_length=4, choices=TYPES,
                            verbose_name=_('Type of paper'), default='PASP')
    series = models.CharField(max_length=100, verbose_name=_('Series'))
    number = models.CharField(max_length=100, verbose_name=_('Number'))
    issued_by = models.CharField(max_length=100,
                                 verbose_name=_('Issued by'))
    date_issued = models.DateField(blank=True, null=True,
                                   verbose_name=_('Date issued'))
    comment = models.TextField(max_length=21500, blank=True, null=True,
                               verbose_name=_('Comment'), default='')
    client = models.ForeignKey(Client, on_delete=models.CASCADE,
                               related_name='papers', verbose_name=_('Client'))

    class Meta:
        verbose_name = _('client paper')
        verbose_name_plural = _('client papers')

    @property
    def full_number(self):
        return ' - '.join([self.series, self.number]).strip()

    def __unicode__(self):
        return ' ' \
            .join((self.series, self.number,)) \
            .strip()

    def __str__(self):
        return self.__unicode__()


class ExpenseType(models.Model):
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))

    title = models.CharField(max_length=50, verbose_name=_('Type of expense'))

    class Meta:
        verbose_name = _('type of expense')
        verbose_name_plural = _('type of expenses')

    def __unicode__(self):
        return self.title

    def __str__(self):
        return self.__unicode__()


class Expense(AuditedObjectMixin, models.Model):
    created_at = models.DateTimeField(auto_now_add=True,
                                      verbose_name=_('Created at'))
    edited_at = models.DateTimeField(auto_now=True, verbose_name=_('Edited at'))

    branch = models.ForeignKey(Branch, verbose_name=_('Branch'), )
    type = models.ForeignKey(ExpenseType, verbose_name=_('Type of expense'))
    hidden = models.BooleanField(verbose_name=_('Is expense hidden?'), default=False)
    cost = models.DecimalField(max_digits=8, decimal_places=2,
                               verbose_name=_('Cost'))
    comment = models.TextField(max_length=21500, blank=True, null=True,
                               verbose_name=_('Comment'))

    class Meta:
        verbose_name = _('expense')
        verbose_name_plural = _('expenses')
        permissions = (
            ('create_hidden_expense', _('Create hidden expense')),
        )

    def __unicode__(self):
        output = _('Expanse: %(type)s %(cost)s.') % {'type': self.type.title, 'cost': self.cost}
        return output

    def __str__(self):
        return self.__unicode__()

    def get_absolute_url(self):
        return reverse('expenses:edit', kwargs={
            'pk': self.id,
        })

    def check_user_access(self, user):
        perm = 'insurance_register_app.create_hidden_expense'

        return not self.hidden or user.has_perm(perm)


class Statistics(models.Model):
    class Meta:
        verbose_name = _('statistics')
        verbose_name_plural = _('statistics')
        managed = False

    @classmethod
    def get_data(cls, conditions):
        ctx = {}

        # type_total = 'total'
        # type_cost_total = 0
        # type_count_total = 0
        payment_types = list(PAYMENT_TYPES)
        insurance_types = OrderedDict(Policy.TYPES)
        branches = Branch.objects.filter(visible__exact=True).all().order_by('sequence', 'name')
        expenses, expenses_hidden, stats_raw = cls._get_stats(conditions)
        expenses = cls._calc_expenses_stats(expenses, expenses_hidden)
        statistics = cls._process_stats(branches=branches,
                                        raw_policies_stats=stats_raw,
                                        expenses=expenses,
                                        insurance_types=insurance_types,
                                        payment_types=payment_types)
        statistics, insurance_types, total = cls._calculate_common_stats(
            statistics,
            payment_types,
            insurance_types
        )

        expenses_total = array_get(expenses, 'expenses_total')
        ctx['statistics'] = statistics
        ctx['types'] = insurance_types
        ctx['branches'] = branches
        ctx['expenses_total'] = expenses_total
        ctx['hidden_total'] = array_get(expenses, 'hidden_total')
        ctx['total_cost'] = total['cost']
        ctx['total_count'] = total['count']
        ctx['total_prolonged_cost'] = total['prolonged_cost']
        ctx['total_prolonged_count'] = total['prolonged_count']
        ctx['total_delta'] = ctx['total_cost'] - (ctx['expenses_total'] + ctx['hidden_total'])
        ctx['title'] = _('Statistics')
        payment_types.insert(0, (PAYMENT_TYPE_ALL, _('All')))
        # payment_types.append((PAYMENT_TYPE_ALL, _('Total')))
        ctx['payment_types'] = payment_types

        return ctx

    @classmethod
    def _process_stats(cls, branches, raw_policies_stats, expenses,
                       insurance_types, payment_types):
        stats = cls._process_raw_stats(raw_policies_stats)

        statistics = []
        for branch in branches:
            branch_statistics = []
            for insurance_type in list(insurance_types.items()):
                # Cost by payment types
                insurance_type_cost = {}
                # Count by payment types
                insurance_type_count = {}
                prolonged_cost, prolonged_count = 0, 0
                for payment_type in payment_types:
                    payment_type = payment_type[0]
                    cost = array_get(stats,
                                     [branch.name, insurance_type[0],
                                      payment_type, 'cost'], 0)
                    count = array_get(stats, [branch.name, insurance_type[0],
                                              payment_type, 'count'], 0)
                    prolonged_cost = array_get(stats,
                                     [branch.name, insurance_type[0],
                                      payment_type, 'prolonged_cost'], 0)
                    prolonged_count = array_get(stats, [branch.name, insurance_type[0],
                                              payment_type, 'prolonged_count'], 0)
                    insurance_type_cost[payment_type] = cost
                    insurance_type_count[payment_type] = count
                    print(prolonged_cost, prolonged_count)

                branch_statistics.append({
                    'type': insurance_type,
                    'branch': branch.name,
                    'cost': insurance_type_cost,
                    'count': insurance_type_count,
                    'prolonged_cost': prolonged_cost,
                    'prolonged_count': prolonged_count,
                })

            expense = array_get(expenses, ['expenses', branch.id], 0) or 0
            hidden = array_get(expenses, ['hidden', branch.id], 0) or 0

            statistics.append({
                'branch': branch.name,
                'branch_id': branch.id,
                'expense': expense,
                'hidden': hidden,
                'data': branch_statistics
            })

        return statistics

    @classmethod
    def _calculate_common_stats(cls, statistics, payment_types, insurance_types):
        # Insurance types stats
        ins_types_stats = cls._get_types_nulls(insurance_types, payment_types)
        total_stat = {
            'cost': 0,
            'count': 0,
            'prolonged_cost': 0,
            'prolonged_count': 0,
        }

        for branch in statistics:
            branch['total'] = {
                'cost': {type_name: 0 for type_name, _ in payment_types},
                'count': {type_name: 0 for type_name, _ in payment_types},
            }
            branch['total']['cost'][PAYMENT_TYPE_ALL] = 0
            branch['total']['count'][PAYMENT_TYPE_ALL] = 0
            branch['total']['prolonged_cost'] = 0
            branch['total']['prolonged_count'] = 0
            # Iterate over insurance types
            for insurance_type in branch['data']:
                insurance_type['cost'][PAYMENT_TYPE_ALL] = 0
                insurance_type['count'][PAYMENT_TYPE_ALL] = 0
                ins_types_stats[insurance_type['type'][0]]['total_prolonged_cost'] += insurance_type['prolonged_cost']
                ins_types_stats[insurance_type['type'][0]]['total_prolonged_count'] += insurance_type['prolonged_count']
                branch['total']['prolonged_cost'] += insurance_type['prolonged_cost']
                branch['total']['prolonged_count'] += insurance_type['prolonged_count']
                total_stat['prolonged_cost'] += insurance_type['prolonged_cost']
                total_stat['prolonged_count'] += insurance_type['prolonged_count']
                for payment_type in payment_types:
                    cost = insurance_type['cost'][payment_type[0]] or 0
                    count = insurance_type['count'][payment_type[0]] or 0

                    insurance_type['cost'][PAYMENT_TYPE_ALL] += cost
                    insurance_type['count'][PAYMENT_TYPE_ALL] += count

                    branch['total']['cost'][payment_type[0]] += cost
                    branch['total']['count'][payment_type[0]] += count

                    branch['total']['cost'][PAYMENT_TYPE_ALL] += cost
                    branch['total']['count'][PAYMENT_TYPE_ALL] += count

                    ins_types_stats[insurance_type['type'][0]]['total'][payment_type[0]]['cost'] += cost
                    ins_types_stats[insurance_type['type'][0]]['total'][payment_type[0]]['count'] += count

                    ins_types_stats[insurance_type['type'][0]]['total'][PAYMENT_TYPE_ALL]['cost'] += cost
                    ins_types_stats[insurance_type['type'][0]]['total'][PAYMENT_TYPE_ALL]['count'] += count

                    total_stat['cost'] += cost
                    total_stat['count'] += count

            total_expense = branch['expense'] + branch['hidden']
            total = branch['total']['cost'][PAYMENT_TYPE_ALL]
            branch['delta'] = total - total_expense

        return statistics, ins_types_stats, total_stat

    @classmethod
    def _process_raw_stats(cls, raw_policies_stats):
        stats = {}
        for stat in raw_policies_stats:
            branch_name = stat['branch__name']
            insurance_type = stat['type']
            payment = stat['payment_type']

            if branch_name not in stats:
                stats[branch_name] = {}
            if insurance_type not in stats[branch_name]:
                stats[branch_name][insurance_type] = {}
            if payment not in stats[branch_name][insurance_type]:
                stats[branch_name][insurance_type][payment] = {}
            if payment not in stats[branch_name][insurance_type]:
                stats[branch_name][insurance_type][payment] = {}

            stats[branch_name][insurance_type][payment]['count'] = stat.get(
                'count', 0)
            stats[branch_name][insurance_type][payment]['cost'] = stat.get(
                'cost', 0)
            stats[branch_name][insurance_type][payment]['prolonged_count'] = stat.get(
                'prolonged_count', 0)
            stats[branch_name][insurance_type][payment]['prolonged_cost'] = stat.get(
                'prolonged_cost', 0)

        return stats

    @classmethod
    def _get_types_nulls(cls, insurance_types, payment_types):
        types = OrderedDict()
        for insurance_type_code, insurance_type_title in list(insurance_types.items()):
            types[insurance_type_code] = {
                'title': insurance_type_title,
                'total_prolonged_cost': 0,
                'total_prolonged_count': 0,
                'total': {
                    PAYMENT_TYPE_ALL: {
                        'cost': 0,
                        'count': 0,
                    }
                }
            }
            for payment_type in payment_types:
                types[insurance_type_code]['total'][payment_type[0]] = {
                    'cost': 0,
                    'count': 0,
                }

        return types

    @classmethod
    def _calc_expenses_stats(cls, expenses, expenses_hidden):
        branch_expenses = {}
        branch_expenses_hidden = {}
        branch_expenses_total = 0
        branch_expenses_hidden_total = 0

        for expense in expenses:
            branch_expenses[expense['branch']] = expense['cost']
            branch_expenses_total += expense['cost']

        for expense in expenses_hidden:
            branch_expenses_hidden[expense['branch']] = expense['cost']
            branch_expenses_hidden_total += expense['cost']

        return {
            'expenses': branch_expenses,
            'expenses_total': branch_expenses_total,
            'hidden': branch_expenses_hidden,
            'hidden_total': branch_expenses_hidden_total,
        }

    @classmethod
    def _get_stats(cls, conditions):
        stats_raw = Policy.objects.values('branch', 'type', 'payment_type',
                                          'branch__name')
        expenses = Expense.objects.values('branch', 'branch__name')
        expenses_hidden = Expense.objects.values('branch', 'branch__name')
        expenses_hidden = expenses_hidden.filter(hidden=True)
        for condition in conditions:
            q_object = Q(**condition)
            stats_raw = stats_raw.filter(q_object)
            field = list(condition.keys())[0]
            value = condition[field]
            field = field.split('__')
            if 'date_issued' == field[0]:
                field = '__'.join(['edited_at', field[1]])
                condition = {field: value}
                q_object = Q(**condition)

            expenses = expenses.filter(q_object)
            expenses_hidden = expenses_hidden.filter(q_object)

        prolonged_count = Sum(models.Func('prolong_parent_id', function='IF', template='%(function)s(%(expressions)s is NULL, 0, 1)'))
        prolonged_cost = Sum(models.Func('prolong_parent_id', function='IF', template='%(function)s(%(expressions)s is NULL, 0, cost)'))

        stats_raw = stats_raw.order_by().annotate(count=Count('id'),
                                                  cost=Sum('cost'),
                                                  prolonged_count=prolonged_count,
                                                  prolonged_cost=prolonged_cost,
                                                  )
        # Issued policies only
        stats_raw = stats_raw.filter(status=Policy.STATUS_ISSUED)
        #
        print((stats_raw.query))

        expenses = expenses.annotate(cost=Sum('cost'))
        expenses_hidden = expenses_hidden.annotate(cost=Sum('cost'))

        return expenses, expenses_hidden, stats_raw

